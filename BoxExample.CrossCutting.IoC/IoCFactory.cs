﻿using BoxExample.IoC.WindsorCastle;
using Box.CrossCutting.IoC;
namespace BoxExample.CrossCutting.IoC
{
    public class IoCFactory
    {
        private static IoCFactory instance;

        public static IoCFactory Instance
        {
            get
            {
                return instance;
            }
        }

        static IoCFactory()
        {
            instance = new IoCFactory();
        }

        private IoCFactory()
        {

        }

        public IContainer WebContainer
        {
            get
            {
                return new IoCWebContainer();
            }
        }

        public void WebInit()
        {
            IoCWebContainer.BuildMapping();
        }
    }
}
