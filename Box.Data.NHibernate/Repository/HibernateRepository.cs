﻿using NHibernate.Linq;
using Box.Core;
using Box.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Box.Specification.LTE;

namespace Box.Data.NHibernate.Repository
{
    public class HibernateRepository<TEntity> :
    Repository<TEntity>,
    IFetchRepository<TEntity>,
    IRepository<TEntity>
    where TEntity : class
    {
        public HibernateRepository(IQueryableUnitOfWork unitOfWork)
          : base(unitOfWork)
        {
        }

        public IFetchQuery<TEntity> QueryFetch() => (IFetchQuery<TEntity>)new HibernateRepository<TEntity>.FetchQuery(this, this.unitOfWork.CreateQueryable<TEntity>());

        public IFetchQuery<TEntity> QueryFetch(ISpecification<TEntity> specification) => (IFetchQuery<TEntity>)new HibernateRepository<TEntity>.FetchQuery(this, this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(specification.SatisfiedBy()));

        public IFetchQuery<TEntity> QueryFetch(Expression<Func<TEntity, bool>> filter) => (IFetchQuery<TEntity>)new HibernateRepository<TEntity>.FetchQuery(this, this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter));

        private HibernateRepository<TEntity>.FetchedResult With<TRelated>(
          IQueryable<TEntity> query,
          Expression<Func<TEntity, TRelated>> relatedObjectSelector)
        {
            return new HibernateRepository<TEntity>.FetchedResult(this, (IQueryable<TEntity>)query.Fetch<TEntity, TRelated>(relatedObjectSelector));
        }

        private HibernateRepository<TEntity>.FetchedResult WithMany<TRelated>(
          IQueryable<TEntity> query,
          Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector)
        {
            return new HibernateRepository<TEntity>.FetchedResult(this, (IQueryable<TEntity>)query.FetchMany<TEntity, TRelated>(relatedObjectSelector));
        }

        private HibernateRepository<TEntity>.FetchedResult ThenWith<TFetch, TRelated>(
          INhFetchRequest<TEntity, TFetch> query,
          Expression<Func<TFetch, TRelated>> relatedObjectSelector)
        {
            return new HibernateRepository<TEntity>.FetchedResult(this, (IQueryable<TEntity>)query.ThenFetch<TEntity, TFetch, TRelated>(relatedObjectSelector));
        }

        private HibernateRepository<TEntity>.FetchedResult ThenWithMany<TFetch, TRelated>(
          INhFetchRequest<TEntity, TFetch> query,
          Expression<Func<TFetch, IEnumerable<TRelated>>> relatedObjectSelector)
        {
            return new HibernateRepository<TEntity>.FetchedResult(this, (IQueryable<TEntity>)query.ThenFetch<TEntity, TFetch, IEnumerable<TRelated>>(relatedObjectSelector));
        }

        public class FetchedResult : IFetchedResult<TEntity>
        {
            private HibernateRepository<TEntity> repository;
            private IQueryable<TEntity> query;

            public FetchedResult(HibernateRepository<TEntity> repository, IQueryable<TEntity> query)
            {
                this.repository = repository;
                this.query = query;
            }

            public IFetchedResult<TEntity> With<TRelated>(
              Expression<Func<TEntity, TRelated>> relatedObjectSelector)
            {
                return (IFetchedResult<TEntity>)this.repository.With<TRelated>(this.query, relatedObjectSelector);
            }

            public IFetchedResult<TEntity> WithMany<TRelated>(
              Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector)
            {
                return (IFetchedResult<TEntity>)this.repository.WithMany<TRelated>(this.query, relatedObjectSelector);
            }

            public IFetchedResult<TEntity> ThenWith<TFetch, TRelated>(
              Expression<Func<TFetch, TRelated>> relatedObjectSelector)
            {
                return (IFetchedResult<TEntity>)this.repository.ThenWith<TFetch, TRelated>((INhFetchRequest<TEntity, TFetch>)this.query, relatedObjectSelector);
            }

            public IFetchedResult<TEntity> ThenWithMany<TFetch, TRelated>(
              Expression<Func<TFetch, IEnumerable<TRelated>>> relatedObjectSelector)
            {
                return (IFetchedResult<TEntity>)this.repository.ThenWithMany<TFetch, TRelated>((INhFetchRequest<TEntity, TFetch>)this.query, relatedObjectSelector);
            }

            public IEnumerable<TEntity> Build() => (IEnumerable<TEntity>)this.query;

            public List<TEntity> ToList() => this.Build().ToList<TEntity>();

            public TEntity First() => this.Build().First<TEntity>();

            public TEntity FirstOrDefault() => this.Build().FirstOrDefault<TEntity>();

            public TEntity Single() => this.Build().Single<TEntity>();

            public TEntity SingleOrDefault() => this.Build().SingleOrDefault<TEntity>();

            public TEntity FirstRoot() => this.Build().ToList<TEntity>().First<TEntity>();

            public TEntity FirstOrDefaultRoot() => this.Build().ToList<TEntity>().FirstOrDefault<TEntity>();

            public TEntity SingleRoot() => this.Build().ToList<TEntity>().Single<TEntity>();

            public TEntity SingleOrDefaultRoot() => this.Build().ToList<TEntity>().SingleOrDefault<TEntity>();
        }

        public class FetchQuery : IFetchQuery<TEntity>
        {
            private HibernateRepository<TEntity> repository;
            private IQueryable<TEntity> query;

            public FetchQuery(HibernateRepository<TEntity> repository, IQueryable<TEntity> query)
            {
                this.repository = repository;
                this.query = query;
            }

            public IFetchQuery<TEntity> Order<S>(
              Expression<Func<TEntity, S>> orderByExpression,
              bool ascending = true)
            {
                if (orderByExpression == null)
                    throw new ArgumentNullException(nameof(orderByExpression), "Order By Expression Cannot Be Null");
                this.query = ascending ? (IQueryable<TEntity>)this.query.OrderBy<TEntity, S>(orderByExpression) : (IQueryable<TEntity>)this.query.OrderByDescending<TEntity, S>(orderByExpression);
                return (IFetchQuery<TEntity>)this;
            }

            public IFetchQuery<TEntity> Paged(int pageIndex, int pageCount)
            {
                if (pageIndex < 0)
                    throw new ArgumentException("Invalid Page Index", nameof(pageIndex));
                if (pageCount <= 0)
                    throw new ArgumentException("Invalid Page Count", nameof(pageCount));
                this.query = this.query.Skip<TEntity>(pageIndex * pageCount).Take<TEntity>(pageCount);
                return (IFetchQuery<TEntity>)this;
            }

            public IFetchQuery<TEntity> Take(int count)
            {
                this.query = count > 0 ? this.query.Take<TEntity>(count) : throw new ArgumentException("Invalid Count", nameof(count));
                return (IFetchQuery<TEntity>)this;
            }

            public IFetchedResult<TEntity> With<TRelated>(
              Expression<Func<TEntity, TRelated>> relatedObjectSelector)
            {
                return (IFetchedResult<TEntity>)this.repository.With<TRelated>(this.query, relatedObjectSelector);
            }

            public IFetchedResult<TEntity> WithMany<TRelated>(
              Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector)
            {
                return (IFetchedResult<TEntity>)this.repository.WithMany<TRelated>(this.query, relatedObjectSelector);
            }

            public IEnumerable<TEntity> Build() => (IEnumerable<TEntity>)this.query;

            public List<TEntity> ToList() => this.Build().ToList<TEntity>();

            public TEntity First() => this.Build().First<TEntity>();

            public TEntity FirstOrDefault() => this.Build().FirstOrDefault<TEntity>();

            public TEntity Single() => this.Build().Single<TEntity>();

            public TEntity SingleOrDefault() => this.Build().SingleOrDefault<TEntity>();

            public TEntity FirstRoot() => this.Build().ToList<TEntity>().First<TEntity>();

            public TEntity FirstOrDefaultRoot() => this.Build().ToList<TEntity>().FirstOrDefault<TEntity>();

            public TEntity SingleRoot() => this.Build().ToList<TEntity>().Single<TEntity>();

            public TEntity SingleOrDefaultRoot() => this.Build().ToList<TEntity>().SingleOrDefault<TEntity>();
        }
    }
}
