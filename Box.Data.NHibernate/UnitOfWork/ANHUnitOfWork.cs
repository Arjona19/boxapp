﻿using NHibernate;
using NHibernate.Linq;
using Box.Core;
using Box.Data.Core;
using System;
using System.Data;
using System.Linq;

namespace Box.Data.NHibernate.UnitOfWork
{
    public abstract class ANHUnitOfWork : INHUnitOfWork, IQueryableUnitOfWork, IUnitOfWork, IDisposable
    {
        protected ANHibernateSessionFactory instance;

        public IUnitOfWork Open()
        {
            lock (this.instance.OpenSession().Transaction)
            {
                if (!this.instance.OpenSession().Transaction.IsActive)
                    this.instance.OpenSession().BeginTransaction(IsolationLevel.ReadCommitted);
            }
            return (IUnitOfWork)this;
        }

        public void Flush()
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Flush();
        }

        public ISession Session => this.instance.Session;

        public void Commit()
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Transaction.Commit();
        }

        public void Rollback()
        {
            if (!this.Session.Transaction.IsActive)
                return;
            this.Session.Transaction.Rollback();
        }

        public TEntity Get<TEntity>(object id) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            return this.Session.Get<TEntity>(id);
        }

        public TEntity Load<TEntity>(object id) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            return this.Session.Load<TEntity>(id);
        }

        public void Refresh<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Refresh((object)item);
        }

        public TEntity UnProxy<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            return (TEntity)this.Session.GetSessionImplementation().PersistenceContext.Unproxy((object)item);
        }

        public void Detach<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Evict((object)item);
        }

        public IQueryable<TEntity> CreateQueryable<TEntity>() where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            return this.Session.Query<TEntity>();
        }

        public void RegisterNew<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Save((object)item);
        }

        public void RegisterDelete<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Delete((object)item);
        }

        public void RegisterUpdate<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Update((object)item);
        }

        public void RegisterMerge<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.Merge<TEntity>(item);
        }

        public void SaveOrUpdate<TEntity>(TEntity item) where TEntity : class
        {
            if (!this.Session.Transaction.IsActive)
                throw new InvalidOperationException("No active transation.");
            this.Session.SaveOrUpdate((object)item);
        }

        public void Dispose()
        {
            if (this.Session.Transaction.IsActive)
                this.Session.Transaction.Rollback();
            this.instance.CloseSession();
        }
    }
}
