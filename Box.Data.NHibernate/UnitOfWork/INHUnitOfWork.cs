﻿using NHibernate;
using Box.Core;
using Box.Data.Core;
using System;

namespace Box.Data.NHibernate.UnitOfWork
{
    public interface INHUnitOfWork : IQueryableUnitOfWork, IUnitOfWork, IDisposable
    {
        ISession Session { get; }
    }
}
