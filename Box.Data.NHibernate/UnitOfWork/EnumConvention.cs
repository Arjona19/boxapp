﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;
using System;

namespace Box.Data.NHibernate.UnitOfWork
{
    public class EnumConvention :
    IUserTypeConvention,
    IPropertyConventionAcceptance,
    IConventionAcceptance<IPropertyInspector>,
    IPropertyConvention,
    IConvention<IPropertyInspector, IPropertyInstance>,
    IConvention
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> crit) => crit.Expect((Func<IPropertyInspector, bool>)(x => x.Property.PropertyType.IsEnum));

        public void Apply(IPropertyInstance instance) => instance.CustomType(instance.Property.PropertyType);
    }
}
