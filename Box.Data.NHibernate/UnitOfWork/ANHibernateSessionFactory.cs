﻿using NHibernate;
using System.Web;

namespace Box.Data.NHibernate.UnitOfWork
{
    public abstract class ANHibernateSessionFactory
    {
        protected object bloqueo;
        private FlushMode flushMode;
        private ISession session;
        private ISessionFactory sessionFactory;

        public ISession Session
        {
            get => this.session;
            private set => this.session = value;
        }

        protected static bool IsWeb => HttpContext.Current != null;

        protected ANHibernateSessionFactory(FlushMode flushMode)
        {
            this.bloqueo = new object();
            this.flushMode = flushMode;
        }

        protected abstract ISessionFactory CreateSessionFactory();

        public ISession OpenSession()
        {
            lock (this.bloqueo)
            {
                if (this.sessionFactory == null)
                    this.sessionFactory = this.CreateSessionFactory();
                if (this.Session == null || !this.Session.Transaction.IsActive)
                {
                    this.Session = this.sessionFactory.OpenSession();
                    this.Session.FlushMode = this.flushMode;
                }
                return this.Session;
            }
        }

        public void CloseSession()
        {
            if (this.Session.IsOpen)
                this.Session.Close();
            this.Session.Dispose();
        }
    }
}
