﻿using Box.Core;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Event;
using System;
namespace Box.Data.NHibernate.Listeners
{
    public class AutoincrementPreInsert : IPreInsertEventListener
    {
        private readonly string _property;

        public AutoincrementPreInsert(string property) => this._property = property;

        public bool OnPreInsert(PreInsertEvent @event)
        {
            if (!(((AbstractPreDatabaseOperationEvent)@event).Entity is IAutoincrement))
                return false;
            int? nullable = (int?)((ISession)((AbstractEvent)@event).Session).GetSession((EntityMode)0).CreateCriteria(((AbstractPreDatabaseOperationEvent)@event).Entity.GetType()).SetProjection(new IProjection[1]
            {
        (IProjection) Projections.Max(this._property)
            }).UniqueResult<int?>();
            IAutoincrement entity = (IAutoincrement)((AbstractPreDatabaseOperationEvent)@event).Entity;
            entity.Autoincrement = new int?(nullable.GetValueOrDefault() + 1);
            int index = Array.FindIndex<string>(((AbstractPreDatabaseOperationEvent)@event).Persister.PropertyNames, (Predicate<string>)(n => n == this._property));
            @event.State[index] = (object)entity.Autoincrement;
            return false;
        }
    }
}
