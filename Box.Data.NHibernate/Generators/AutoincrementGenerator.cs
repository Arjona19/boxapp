﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.Id;

namespace Box.Data.NHibernate
{
    public class AutoincrementGenerator<T> : IIdentifierGenerator where T : class
    {
        private readonly string _property;

        public AutoincrementGenerator(string property) => this._property = property;

        public object Generate(ISessionImplementor session, object obj) => (object)(((int?)((ISession)session).CreateCriteria(typeof(T)).SetProjection(new IProjection[1]
        {
      (IProjection) Projections.Max(this._property)
        }).UniqueResult<int?>()).GetValueOrDefault() + 1);
    }
}
