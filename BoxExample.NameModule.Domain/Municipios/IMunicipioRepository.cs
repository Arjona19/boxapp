﻿using Box.Core;
using Box.Specification.LTE;
using System;
using System.Linq.Expressions;

namespace BoxExample.NameModule.Domain.Municipios
{
    public interface IMunicipioRepository : IRepository<Municipio>
    {
        IFetchQuery<Municipio> QueryFetch(ISpecification<Municipio> filter);
        IFetchQuery<Municipio> QueryFetch(Expression<Func<Municipio, bool>> filter);
    }
}
