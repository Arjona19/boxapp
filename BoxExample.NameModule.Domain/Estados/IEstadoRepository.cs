﻿using Box.Core;
using Box.Specification.LTE;
using System;
using System.Linq.Expressions;

namespace BoxExample.NameModule.Domain.Estados
{
    public interface IEstadoRepository : IRepository<Estado>
    {
        IFetchQuery<Estado> QueryFetch(ISpecification<Estado> filter);
        IFetchQuery<Estado> QueryFetch(Expression<Func<Estado, bool>> filter);
    }
}
