﻿
namespace BoxExample.NameModule.Domain
{
    public class Municipio
    {
        public virtual int? MunicipioID { get; set; }
        public virtual string Nombre { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual int? EstadoID { get { return this.Estado?.EstadoID; } set { } }
    }
}
