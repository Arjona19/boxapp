﻿
namespace BoxExample.NameModule.Domain
{
    public class Localidad
    {
        public virtual int? LocalidadID { get; set; }
        public virtual string Nombre { get; set; }
        public virtual Municipio Municipio { get; set; }
        public virtual int? MunicipioID { get { return this.Municipio?.MunicipioID; } set { } }
    }
}
