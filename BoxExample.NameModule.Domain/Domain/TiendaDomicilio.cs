﻿using System;

namespace BoxExample.NameModule.Domain
{
    public class TiendaDomicilio : ADomicilio
    {
        public virtual string Nombre { get; set; }
        public virtual ETIpoTienda? TipoTienda { get; set; }
        public virtual bool? EstaActivo { get; set; }

        public virtual Tienda Tienda { get; set; }

        public virtual Guid? TiendaID { get { return this.Tienda?.TiendaID; } set { } }
    }
}
