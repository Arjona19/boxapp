﻿using System;

namespace BoxExample.NameModule.Domain
{
    public abstract class ADomicilio
    {
        public virtual Guid? DomicilioID { get; set; }
        public virtual string Direccion { get; set; }
        public virtual string Colonia { get; set; }
        public virtual int? CP { get; set; }
        public virtual string NumExt { get; set; }
        public virtual string NumInt { get; set; }
        public virtual string Telefono { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual Municipio Municipio { get; set; }
        public virtual Localidad Localidad { get; set; }

        public virtual int? EstadoID { get { return this.Estado?.EstadoID; } set { } }
        public virtual int? MunicipioID { get { return this.Municipio?.MunicipioID; } set { } }
        public virtual int? LocalidadID { get { return this.Localidad?.LocalidadID; } set { } }

    }
}
