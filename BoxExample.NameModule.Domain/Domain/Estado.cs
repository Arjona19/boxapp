﻿
namespace BoxExample.NameModule.Domain
{
    public class Estado
    {
        public virtual int? EstadoID { get; set; }
        public virtual string Nombre { get; set; }
    }
}
