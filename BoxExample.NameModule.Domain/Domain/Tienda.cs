﻿using System;
using System.Collections.Generic;

namespace BoxExample.NameModule.Domain
{
    public class Tienda
    {
        private IList<TiendaDomicilio> tiendaDomicilios;

        public Tienda()
        {
            tiendaDomicilios = new List<TiendaDomicilio>();
        }

        public virtual Guid? TiendaID { get; set; }
        public virtual string RFC { get; set; }
        public virtual string RazonSocial { get; set; }
        public virtual string NombreComercial { get; set; }
        public virtual string Email { get; set; }
        public virtual string Telefono { get; set; }

        public virtual IEnumerable<TiendaDomicilio> TiendaDomicilios
        {
            get { return tiendaDomicilios; }
            set { tiendaDomicilios = (IList<TiendaDomicilio>)value; }
        }
    }
}
