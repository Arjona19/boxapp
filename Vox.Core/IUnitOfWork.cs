﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Box.Core
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();

        void Detach<TEntity>(TEntity item)
        where TEntity : class;

        void Flush();

        TEntity Get<TEntity>(object id)
        where TEntity : class;

        TEntity Load<TEntity>(object id)
        where TEntity : class;

        IUnitOfWork Open();

        void Refresh<TEntity>(TEntity item)
        where TEntity : class;

        void RegisterDelete<TEntity>(TEntity item)
        where TEntity : class;

        void RegisterNew<TEntity>(TEntity item)
        where TEntity : class;

        void RegisterUpdate<TEntity>(TEntity item)
        where TEntity : class;

        void Rollback();

        void SaveOrUpdate<TEntity>(TEntity item)
        where TEntity : class;

        TEntity UnProxy<TEntity>(TEntity item)
        where TEntity : class;
    }
}
