﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Box.Core
{
    public interface IFetchedResult<TEntity>
    where TEntity : class
    {
        IEnumerable<TEntity> Build();

        TEntity First();

        TEntity FirstOrDefault();

        TEntity FirstOrDefaultRoot();

        TEntity FirstRoot();

        TEntity Single();

        TEntity SingleOrDefault();

        TEntity SingleOrDefaultRoot();

        TEntity SingleRoot();

        IFetchedResult<TEntity> ThenWith<TFetch, TRelated>(Expression<Func<TFetch, TRelated>> relatedObjectSelector);

        IFetchedResult<TEntity> ThenWithMany<TFetch, TRelated>(Expression<Func<TFetch, IEnumerable<TRelated>>> relatedObjectSelector);

        List<TEntity> ToList();

        IFetchedResult<TEntity> With<TRelated>(Expression<Func<TEntity, TRelated>> relatedObjectSelector);

        IFetchedResult<TEntity> WithMany<TRelated>(Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector);
    }
}
