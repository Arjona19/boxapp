﻿using Box.Specification.LTE;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
namespace Box.Core
{
    public interface IRepository<TEntity>
    where TEntity : class
    {
        IUnitOfWork UnitOfWork
        {
            get;
        }

        void Add(TEntity item);

        IEnumerable<TEntity> All();

        bool All(ISpecification<TEntity> specification);

        bool All(Expression<Func<TEntity, bool>> filter);

        bool Any(ISpecification<TEntity> specification);

        bool Any(Expression<Func<TEntity, bool>> filter);

        long Count(ISpecification<TEntity> specification);

        long Count(Expression<Func<TEntity, bool>> filter);

        IEnumerable<TEntity> FilterElements(Expression<Func<TEntity, bool>> filter);

        IEnumerable<TEntity> FilterElements<S>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, S>> orderByExpression, bool ascending = true);

        IEnumerable<TEntity> FilterElements<S>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, S>> orderByExpression, int pageIndex, int pageCount, bool ascending = true);

        IEnumerable<TEntity> FindBy(ISpecification<TEntity> specification);

        IEnumerable<TEntity> PagedElements<S>(int pageIndex, int pageCount, Expression<Func<TEntity, S>> orderByExpression, bool ascending = true);

        IEnumerable<TEntity> PagedElements<S>(int pageIndex, int pageCount, Expression<Func<TEntity, S>> orderByExpression, ISpecification<TEntity> specification, bool ascending = true);

        void Remove(TEntity item);

        void SaveOrUpdate(TEntity item);

        int? Sum(Expression<Func<TEntity, int?>> sum);

        int? Sum(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, int?>> sum);

        long? Sum(Expression<Func<TEntity, long?>> sum);

        long? Sum(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, long?>> sum);

        decimal? Sum(Expression<Func<TEntity, decimal?>> sum);

        decimal? Sum(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, decimal?>> sum);

        decimal Sum(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, decimal>> sum);

        void Update(TEntity item);
    }
}
