﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Box.Core
{
    public interface IFetchQuery<TEntity>
   where TEntity : class
    {
        IEnumerable<TEntity> Build();

        TEntity First();

        TEntity FirstOrDefault();

        TEntity FirstOrDefaultRoot();

        TEntity FirstRoot();

        IFetchQuery<TEntity> Order<S>(Expression<Func<TEntity, S>> orderByExpression, bool ascending = true);

        IFetchQuery<TEntity> Paged(int pageIndex, int pageCount);

        TEntity Single();

        TEntity SingleOrDefault();

        TEntity SingleOrDefaultRoot();

        TEntity SingleRoot();

        IFetchQuery<TEntity> Take(int count);

        List<TEntity> ToList();

        IFetchedResult<TEntity> With<TRelated>(Expression<Func<TEntity, TRelated>> relatedObjectSelector);

        IFetchedResult<TEntity> WithMany<TRelated>(Expression<Func<TEntity, IEnumerable<TRelated>>> relatedObjectSelector);
    }
}
