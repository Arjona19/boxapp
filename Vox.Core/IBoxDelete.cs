﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Box.Core
{
    public interface IBoxDelete
    {
        bool? IsDelete
        {
            get;
            set;
        }
    }
}
