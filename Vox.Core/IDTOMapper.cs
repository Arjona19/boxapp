﻿using System.Collections.Generic;

namespace Box.Core
{
    public interface IDTOMapper<TSource, TDestination>
      where TSource : class
      where TDestination : class
    {
        TDestination Convert(TSource source);

        List<TDestination> Convert(List<TSource> source);

        TSource Convert(TDestination source);

        List<TSource> Convert(List<TDestination> source);
    }
}
