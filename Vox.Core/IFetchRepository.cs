﻿using Box.Specification.LTE;
using System;
using System.Linq.Expressions;
using System.Text;

namespace Box.Core
{
    public interface IFetchRepository<TEntity> : IRepository<TEntity>
     where TEntity : class
    {
        IFetchQuery<TEntity> QueryFetch();

        IFetchQuery<TEntity> QueryFetch(ISpecification<TEntity> specification);

        IFetchQuery<TEntity> QueryFetch(Expression<Func<TEntity, bool>> filter);

    }
}
