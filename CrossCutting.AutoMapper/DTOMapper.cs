﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vox.Core;

namespace CrossCutting.AutoMapper
{
    public class DTOMapper<TSource, TDestination> : IDTOMapper<TSource, TDestination>
         where TSource : class
         where TDestination : class
    {
        protected ConfigurationStore config;
        protected MappingEngine engine;

        public DTOMapper() { }

        public virtual TDestination Convert(TSource source);
        public virtual List<TDestination> Convert(List<TSource> source);
        public virtual TSource Convert(TDestination source);
        public virtual List<TSource> Convert(List<TDestination> source);
        protected virtual void DestinationToSourceMap();
        protected virtual void SourceToDestinationMap();
    }
}
