﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Box.CrossCutting.IoC
{
    public interface IContainer: IDisposable
    {
        TService Resolve<TService>();

        TService Resolve<TService>(object argumentsConstructor);

        object Resolve(Type type);

        object Resolve(Type type, object argumentsConstructor);

        void RegisterType<TImpl>(params Action<TImpl>[] actions) where TImpl : class;

        void RegisterType(Type type);

        void RegisterTypeSingleton<TImpl>(params Action<TImpl>[] actions) where TImpl : class;

        void RegisterTypeSingleton(Type type);

        void RegisterType<TDef, TImpl>(params Action<TDef>[] actions)
          where TDef : class
          where TImpl : class, TDef;

        void RegisterType(Type forType, Type implementedBy);

        void RegisterTypeSingleton<TDef, TImpl>(params Action<TDef>[] actions)
          where TDef : class
          where TImpl : class, TDef;

        void RegisterTypeSingleton(Type forType, Type implementedBy);

        void RegisterTypeWithInstance(Type forType, object instance);
    }
}
