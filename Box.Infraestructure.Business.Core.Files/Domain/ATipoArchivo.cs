﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Box.Infraestructure.Business.Core.Files.Domain
{
    public abstract class ATipoArchivo
    {
        private int? tipoArchivoID;
        private string extension;
        private string mimeType;
        private string descripcion;
        private bool? activo;
        private bool? esImagen;

        public virtual int? TipoArchivoID
        {
            get => this.tipoArchivoID;
            set => this.tipoArchivoID = value;
        }

        public virtual string Extension
        {
            get => this.extension;
            set => this.extension = value;
        }

        public virtual string MimeType
        {
            get => this.mimeType;
            set => this.mimeType = value;
        }

        public virtual string Descripcion
        {
            get => this.descripcion;
            set => this.descripcion = value;
        }

        public virtual bool? Activo
        {
            get => this.activo;
            set => this.activo = value;
        }

        public virtual bool? EsImagen
        {
            get => this.esImagen;
            set => this.esImagen = value;
        }
    }
}
