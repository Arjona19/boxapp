﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Box.Infraestructure.Business.Core.Files.Domain
{
    public abstract class ADocumento
    {
        private Guid? documentoID;
        private string nombre;
        private DateTime? fecha;
        private byte[] version;
        private ACarpetaDocumentos carpetaDocumentos;
        private AArchivo archivo;

        public virtual Guid? DocumentoID
        {
            get => this.documentoID;
            set => this.documentoID = value;
        }

        public virtual string Nombre
        {
            get => this.nombre;
            set => this.nombre = value;
        }

        public virtual DateTime? Fecha
        {
            get => this.fecha;
            set => this.fecha = value;
        }

        public virtual byte[] Version
        {
            get => this.version;
            set => this.version = value;
        }

        public virtual ACarpetaDocumentos CarpetaDocumentos
        {
            get => this.carpetaDocumentos;
            set => this.carpetaDocumentos = value;
        }

        public virtual AArchivo Archivo
        {
            get => this.archivo;
            set => this.archivo = value;
        }

        public virtual Guid? CarpetaDocumentosID
        {
            get => this.CarpetaDocumentos != null ? this.CarpetaDocumentos.CarpetaDocumentosID : new Guid?();
            protected set
            {
            }
        }

        public virtual Guid? ArchivoID
        {
            get => this.Archivo != null ? this.Archivo.ArchivoID : new Guid?();
            protected set
            {
            }
        }
    }
}
