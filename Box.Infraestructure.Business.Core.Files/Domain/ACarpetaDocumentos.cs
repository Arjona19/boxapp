﻿using Box.Infraestructure.Business.Core.Files.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Box.Infraestructure.Business.Core.Files.Domain
{
    public abstract class ACarpetaDocumentos
    {
        private Guid? carpetaDocumentosID;
        private string nombre;
        private bool? activo;
        private byte[] version;

        public virtual Guid? CarpetaDocumentosID
        {
            get => this.carpetaDocumentosID;
            set => this.carpetaDocumentosID = value;
        }

        public virtual string Nombre
        {
            get => this.nombre;
            set => this.nombre = value;
        }

        public virtual bool? Activo
        {
            get => this.activo;
            set => this.activo = value;
        }

        public virtual byte[] Version
        {
            get => this.version;
            set => this.version = value;
        }

        public abstract IEnumerable<ADocumento> Documentos { get; protected set; }
    }
}
