﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Box.Infraestructure.Business.Core.Files.Domain
{
    public abstract class AArchivo
    {
        private Guid? archivoID;
        private string nombre;
        private string ruta;
        private byte[] contenido;
        private ATipoArchivo tipoArchivo;
        private byte[] version;

        public virtual Guid? ArchivoID
        {
            get => this.archivoID;
            set => this.archivoID = value;
        }

        public virtual string Nombre
        {
            get => this.nombre;
            set => this.nombre = value;
        }

        public virtual string Ruta
        {
            get => this.ruta;
            set => this.ruta = value;
        }

        public virtual byte[] Contenido
        {
            get => this.contenido;
            set => this.contenido = value;
        }

        public virtual ATipoArchivo TipoArchivo
        {
            get => this.tipoArchivo;
            set => this.tipoArchivo = value;
        }

        public virtual byte[] Version
        {
            get => this.version;
            set => this.version = value;
        }

        public virtual int? TipoArchivoID
        {
            get => this.TipoArchivo != null ? this.TipoArchivo.TipoArchivoID : new int?();
            protected set
            {
            }
        }
    }
}
