﻿using Box.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Box.Data.Core
{
    public interface IQueryableUnitOfWork : IUnitOfWork, IDisposable
    {
        IQueryable<TEntity> CreateQueryable<TEntity>() where TEntity : class;
    }
}
