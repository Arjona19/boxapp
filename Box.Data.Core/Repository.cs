﻿using Box.Core;
using Box.Specification.LTE;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Box.Data.Core
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected IQueryableUnitOfWork unitOfWork;

        public Repository(IQueryableUnitOfWork unitOfWork) => this.unitOfWork = unitOfWork;

        public IUnitOfWork UnitOfWork => (IUnitOfWork)this.unitOfWork;

        public virtual void Add(TEntity item) => this.unitOfWork.RegisterNew<TEntity>(item);

        public virtual void Remove(TEntity item) => this.unitOfWork.RegisterDelete<TEntity>(item);

        public virtual void Update(TEntity item) => this.unitOfWork.RegisterUpdate<TEntity>(item);

        public virtual void SaveOrUpdate(TEntity item) => this.unitOfWork.SaveOrUpdate<TEntity>(item);

        public virtual IEnumerable<TEntity> All() => (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>();

        public virtual long Count(ISpecification<TEntity> specification) => specification != null ? this.unitOfWork.CreateQueryable<TEntity>().LongCount<TEntity>(specification.SatisfiedBy()) : throw new ArgumentNullException(nameof(specification));

        public virtual bool Any(ISpecification<TEntity> specification) => specification != null ? this.unitOfWork.CreateQueryable<TEntity>().Any<TEntity>(specification.SatisfiedBy()) : throw new ArgumentNullException(nameof(specification));

        public virtual bool All(ISpecification<TEntity> specification) => specification != null ? this.unitOfWork.CreateQueryable<TEntity>().All<TEntity>(specification.SatisfiedBy()) : throw new ArgumentNullException(nameof(specification));

        public virtual IEnumerable<TEntity> FindBy(ISpecification<TEntity> specification) => specification != null ? (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(specification.SatisfiedBy()) : throw new ArgumentNullException(nameof(specification));

        public virtual IEnumerable<TEntity> PagedElements<S>(
          int pageIndex,
          int pageCount,
          Expression<Func<TEntity, S>> orderByExpression,
          bool ascending = true)
        {
            if (pageIndex < 0)
                throw new ArgumentException("Invalid Page Index", nameof(pageIndex));
            if (pageCount <= 0)
                throw new ArgumentException("Invalid Page Count", nameof(pageCount));
            if (orderByExpression == null)
                throw new ArgumentNullException(nameof(orderByExpression), "Order By Expression Cannot Be Null");
            return !ascending ? (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().OrderByDescending<TEntity, S>(orderByExpression).Skip<TEntity>(pageIndex * pageCount).Take<TEntity>(pageCount) : (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().OrderBy<TEntity, S>(orderByExpression).Skip<TEntity>(pageIndex * pageCount).Take<TEntity>(pageCount);
        }

        public virtual IEnumerable<TEntity> PagedElements<S>(
          int pageIndex,
          int pageCount,
          Expression<Func<TEntity, S>> orderByExpression,
          ISpecification<TEntity> specification,
          bool ascending = true)
        {
            if (pageIndex < 0)
                throw new ArgumentException("Invalid Page Index", nameof(pageIndex));
            if (pageCount <= 0)
                throw new ArgumentException("Invalid Page Count", nameof(pageCount));
            if (orderByExpression == null)
                throw new ArgumentNullException(nameof(orderByExpression), "Order By Expression Cannot Be Null");
            if (specification == null)
                throw new ArgumentNullException(nameof(specification), "Specificacion Is Null.");
            return !ascending ? (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(specification.SatisfiedBy()).OrderByDescending<TEntity, S>(orderByExpression).Skip<TEntity>(pageIndex * pageCount).Take<TEntity>(pageCount) : (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(specification.SatisfiedBy()).OrderBy<TEntity, S>(orderByExpression).Skip<TEntity>(pageIndex * pageCount).Take<TEntity>(pageCount);
        }

        public virtual long Count(Expression<Func<TEntity, bool>> filter) => filter != null ? this.unitOfWork.CreateQueryable<TEntity>().LongCount<TEntity>(filter) : throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");

        public virtual bool Any(Expression<Func<TEntity, bool>> filter) => filter != null ? this.unitOfWork.CreateQueryable<TEntity>().Any<TEntity>(filter) : throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");

        public virtual bool All(Expression<Func<TEntity, bool>> filter) => filter != null ? this.unitOfWork.CreateQueryable<TEntity>().All<TEntity>(filter) : throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");

        public virtual int? Sum(Expression<Func<TEntity, int?>> sum) => sum != null ? this.unitOfWork.CreateQueryable<TEntity>().Sum<TEntity>(sum) : throw new ArgumentNullException("filter", "Filter Cannot Be Null.");

        public virtual int? Sum(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, int?>> sum)
        {
            if (sum == null)
                throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");
            return this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).Sum<TEntity>(sum);
        }

        public virtual long? Sum(Expression<Func<TEntity, long?>> sum) => sum != null ? this.unitOfWork.CreateQueryable<TEntity>().Sum<TEntity>(sum) : throw new ArgumentNullException("filter", "Filter Cannot Be Null.");

        public virtual long? Sum(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, long?>> sum)
        {
            if (sum == null)
                throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");
            return this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).Sum<TEntity>(sum);
        }

        public virtual Decimal? Sum(Expression<Func<TEntity, Decimal?>> sum) => sum != null ? this.unitOfWork.CreateQueryable<TEntity>().Sum<TEntity>(sum) : throw new ArgumentNullException("filter", "Filter Cannot Be Null.");

        public virtual Decimal? Sum(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, Decimal?>> sum)
        {
            if (sum == null)
                throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");
            return this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).Sum<TEntity>(sum);
        }

        public virtual Decimal Sum(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, Decimal>> sum)
        {
            if (sum == null)
                throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");
            return this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).Sum<TEntity>(sum);
        }

        public virtual IEnumerable<TEntity> FilterElements(
          Expression<Func<TEntity, bool>> filter)
        {
            return filter != null ? (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter) : throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null.");
        }

        public virtual IEnumerable<TEntity> FilterElements<S>(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, S>> orderByExpression,
          bool ascending = true)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null");
            if (orderByExpression == null)
                throw new ArgumentNullException(nameof(orderByExpression), "Order By Expression Cannot Be Null");
            return !ascending ? (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).OrderByDescending<TEntity, S>(orderByExpression) : (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).OrderBy<TEntity, S>(orderByExpression);
        }

        public virtual IEnumerable<TEntity> FilterElements<S>(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, S>> orderByExpression,
          int pageIndex,
          int pageCount,
          bool ascending = true)
        {
            if (filter == null)
                throw new ArgumentNullException(nameof(filter), "Filter Cannot Be Null");
            if (pageIndex < 0)
                throw new ArgumentException("Invalid Page Index", nameof(pageIndex));
            if (pageCount <= 0)
                throw new ArgumentException("Invalid Page Count", nameof(pageCount));
            if (orderByExpression == null)
                throw new ArgumentNullException(nameof(orderByExpression), "Order By Expression Cannot Be Null");
            return !ascending ? (IEnumerable<TEntity>)this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).OrderByDescending<TEntity, S>(orderByExpression).Skip<TEntity>(pageIndex * pageCount).Take<TEntity>(pageCount) : (IEnumerable<TEntity>)
                this.unitOfWork.CreateQueryable<TEntity>().Where<TEntity>(filter).OrderBy<TEntity, S>(orderByExpression).Skip<TEntity>(pageIndex * pageCount).Take<TEntity>(pageCount);
        }
    }
}
