﻿using AutoMapper;
using AutoMapper.Mappers;
using Box.Core;
using System;
using System.Collections.Generic;


namespace Box.CrossCutting.Mapper.AutoMapper
{
    public class DTOMapper<TSource, TDestination> : IDTOMapper<TSource, TDestination>
      where TSource : class
      where TDestination : class
    {
        protected ConfigurationStore config;
        protected MappingEngine engine;

        public DTOMapper()
        {
            this.config = new ConfigurationStore((ITypeMapFactory)new TypeMapFactory(), (IEnumerable<IObjectMapper>)MapperRegistry.Mappers);
            this.engine = new MappingEngine((IConfigurationProvider)this.config);
            this.SourceToDestinationMap();
            this.DestinationToSourceMap();
        }

        protected virtual void SourceToDestinationMap() => this.config.CreateMap<TSource, TDestination>();

        protected virtual void DestinationToSourceMap() => this.config.CreateMap<TSource, TSource>();

        public virtual TDestination Convert(TSource source) => (TDestination)this.engine.Map<TSource, TDestination>((TSource)source, (Action<IMappingOperationOptions<TSource, TDestination>>)(opt => ((IMappingOperationOptions)opt).CreateMissingTypeMaps = true));

        public virtual List<TDestination> Convert(List<TSource> source) => (List<TDestination>)this.engine.Map<List<TSource>, List<TDestination>>((List<TSource>)source, (Action<IMappingOperationOptions<List<TSource>, List<TDestination>>>)(opt => ((IMappingOperationOptions)opt).CreateMissingTypeMaps = true));

        public virtual TSource Convert(TDestination source) => (TSource)this.engine.Map<TDestination, TSource>((TDestination)source, (Action<IMappingOperationOptions<TDestination, TSource>>)(opt => ((IMappingOperationOptions)opt).CreateMissingTypeMaps = true));

        public virtual List<TSource> Convert(List<TDestination> source) => (List<TSource>)this.engine.Map<List<TDestination>, List<TSource>>((List<TDestination>)source, (Action<IMappingOperationOptions<List<TDestination>, List<TSource>>>)(opt => ((IMappingOperationOptions)opt).CreateMissingTypeMaps = true));
    }
}
