﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CantidadLetra
{
    public class CantidadLetra
    {
        private string[] sUnidades = new string[30]
        {
    "",
    "un",
    "dos",
    "tres",
    "cuatro",
    "cinco",
    "seis",
    "siete",
    "ocho",
    "nueve",
    "diez",
    "once",
    "doce",
    "trece",
    "catorce",
    "quince",
    "dieciseis",
    "diecisiete",
    "dieciocho",
    "diecinueve",
    "veinte",
    "veintiún",
    "veintidos",
    "veintitres",
    "veinticuatro",
    "veinticinco",
    "veintiseis",
    "veintisiete",
    "veintiocho",
    "veintinueve"
        };
        private string[] sDecenas = new string[10]
        {
    "",
    "diez",
    "veinte",
    "treinta",
    "cuarenta",
    "cincuenta",
    "sesenta",
    "setenta",
    "ochenta",
    "noventa"
        };
        private string[] sCentenas = new string[10]
        {
    "cien",
    "ciento",
    "doscientos",
    "trescientos",
    "cuatrocientos",
    "quinientos",
    "seiscientos",
    "setecientos",
    "ochocientos",
    "novecientos"
        };
        private string sResultado = "";

        public string ConvertirCadena(string sNumero, string moneda, string MN)
        {
            this.sResultado = " ";
            double num;
            try
            {
                num = Convert.ToDouble(sNumero);
            }
            catch
            {
                return "";
            }
            if (num >= 1000000000000.0)
                return "";
            string str = num.ToString().Split('.')[0];
            if (num >= 1000000000.0)
            {
                double dNumAux = (double)(Convert.ToInt64(str) % 1000000000000L);
                CantidadLetra cantidadLetra = this;
                cantidadLetra.sResultado = cantidadLetra.sResultado + this.Numeros(dNumAux, 1000000000.0) + "mil ";
            }
            if (num >= 1000000.0)
            {
                double dNumAux = (double)(Convert.ToInt64(str) % 1000000000L);
                if (dNumAux == 1000000.0)
                {
                    CantidadLetra cantidadLetra = this;
                    cantidadLetra.sResultado = cantidadLetra.sResultado + this.Numeros(dNumAux, 1000000.0) + "millon";
                }
                else
                {
                    CantidadLetra cantidadLetra = this;
                    cantidadLetra.sResultado = cantidadLetra.sResultado + this.Numeros(dNumAux, 1000000.0) + "millones ";
                }
            }
            if (num >= 1000.0)
            {
                double dNumAux = (double)(Convert.ToInt64(str) % 1000000L);
                if (dNumAux != 0.0)
                {
                    if (dNumAux < 2000.0)
                    {
                        this.sResultado += " mil ";
                    }
                    else
                    {
                        CantidadLetra cantidadLetra = this;
                        cantidadLetra.sResultado = cantidadLetra.sResultado + this.Numeros(dNumAux, 1000.0) + "mil ";
                    }
                }
            }
            this.sResultado += this.Numeros((double)(Convert.ToInt64(str) % 1000L), 1.0);
            string sNumero1 = num.ToString();
            if (this.sResultado.Trim().EndsWith("millones") || this.sResultado.Trim().EndsWith("millon"))
                this.sResultado += "de ";
            CantidadLetra cantidadLetra1 = this;
            cantidadLetra1.sResultado = cantidadLetra1.sResultado + moneda + " ";
            if (sNumero1.IndexOf(".") >= 0)
            {
                this.sResultado += this.ObtenerDecimales(sNumero1, MN);
            }
            else
            {
                this.sResultado += "00/100";
                if (MN.ToUpper() == "MX")
                    this.sResultado += " M.N";
                else
                    this.sResultado += " M.E";
            }
            string sResultado = this.sResultado;
            this.sResultado = char.ToUpper(this.sResultado[1]).ToString();
            for (int index = 2; index < sResultado.Length; ++index)
                this.sResultado += sResultado[index].ToString();
            return this.sResultado;
        }

        public string ConvertirCadena(double dNumero, string moneda, string MN) => this.ConvertirCadena(dNumero.ToString(), moneda, MN);

        private string Numeros(double dNumAux, double dFactor)
        {
            double num1 = dNumAux / dFactor;
            int num2 = 0;
            string str1 = "";
            char ch;
            if (num1 >= 100.0 && num1 < 101.0)
            {
                ch = (num1 / 100.0).ToString()[0];
                num2 = int.Parse(ch.ToString());
                str1 = str1 + this.sCentenas[0] + " ";
            }
            else if (num1 >= 101.0)
            {
                ch = (num1 / 100.0).ToString()[0];
                int index = int.Parse(ch.ToString());
                str1 = str1 + this.sCentenas[index] + " ";
            }
            double num3 = num1 % 100.0;
            if (num3 >= 30.0)
            {
                ch = (num3 / 10.0).ToString()[0];
                int index1 = int.Parse(ch.ToString());
                if (index1 > 0)
                    str1 = str1 + this.sDecenas[index1] + " ";
                ch = (num3 % 10.0).ToString()[0];
                int index2 = int.Parse(ch.ToString());
                if (index2 > 0)
                    str1 = str1 + "y " + this.sUnidades[index2] + " ";
            }
            else
            {
                string str2 = num3.ToString();
                int index;
                if (str2.Length > 1)
                {
                    if (str2[1] != '.')
                    {
                        ch = str2[0];
                        string str3 = ch.ToString();
                        ch = str2[1];
                        string str4 = ch.ToString();
                        index = int.Parse(str3 + str4);
                    }
                    else
                    {
                        ch = str2[0];
                        index = int.Parse(ch.ToString());
                    }
                }
                else
                {
                    ch = str2[0];
                    index = int.Parse(ch.ToString());
                }
                str1 = str1 + this.sUnidades[index] + " ";
            }
            return str1;
        }

        private string ObtenerDecimales(string sNumero, string esMN)
        {
            string[] strArray = sNumero.Split('.');
            string str = strArray[1].Length <= 1 ? strArray[1] + "0/100" : strArray[1].Substring(0, 2) + "/100";
            return !(esMN.ToUpper() == "MX") ? str + " M.E." : str + " M.N.";
        }
    }
}
