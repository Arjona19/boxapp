﻿using System;
using System.Configuration;
using System.Reflection;
using System.IO;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Box.Data.NHibernate.UnitOfWork;
using FluentNHibernate.Automapping;
using FluentNHibernate.Data;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Cfg;

namespace BoxApp.Data.NHibernate
{
    public class BoxExampleSessionFactory : ANHibernateSessionFactory
    {
        #region Singleton
        private static BoxExampleSessionFactory nHibernateSessionFactory = null;
        private static ISessionFactory singletonSessionFactory = null;

        private BoxExampleSessionFactory() : base(FlushMode.Auto)
        {
        }

        static BoxExampleSessionFactory()
        {
            nHibernateSessionFactory = new BoxExampleSessionFactory();
        }

        public static BoxExampleSessionFactory Instance
        {
            get { return nHibernateSessionFactory; }
        }
        #endregion

        public static void BuildMapping()
        {
            singletonSessionFactory = nHibernateSessionFactory.CreateSessionFactory();
        }

        public static BoxExampleSessionFactory Create()
        {
            return new BoxExampleSessionFactory();
        }

        protected override ISessionFactory CreateSessionFactory()
        {
            if (singletonSessionFactory != null)
                return singletonSessionFactory;

            //Agregacion de Migraciones. Comentar en caso de desactivarlos.
            var autoMap = AutoMap.AssemblyOf<Entity>()
                                 .Where(t => typeof(Entity).IsAssignableFrom(t));

            string ruta = AppDomain.CurrentDomain.BaseDirectory;
            if (BoxExampleSessionFactory.IsWeb)
                ruta = Path.Combine(ruta, "bin");
            else if (ConfigurationManager.AppSettings["IsWeb"] != null && bool.Parse(ConfigurationManager.AppSettings["IsWeb"]))
                ruta = Path.Combine(ruta, "bin");
            
            string connection = ConfigurationManager.ConnectionStrings["BoxExampleMsSqlServerConnection"].ConnectionString;

            var db = MsSqlConfiguration.MsSql2008.ConnectionString(connection);

#if DEBUG
            db.ShowSql().FormatSql();
#endif
            //Registrar los dlls de los modulos existentes
            var config = Fluently.Configure().Database(db)
                .Mappings(m=> m.AutoMappings.Add(autoMap))
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.LoadFrom(Path.Combine(ruta, "BoxApp.NameModule.Data.dll"))))
                .Mappings(m => m.FluentMappings.Conventions.AddFromAssemblyOf<EnumConvention>())
                .ExposeConfiguration(TreatConfiguration);

#if DEBUG
            config.Diagnostics(x => x.Enable());
#endif

            return config.BuildSessionFactory();
        }

        //Actualiza la bd desde los modelos
        private void TreatConfiguration(global::NHibernate.Cfg.Configuration configuration)
        {
            var update = new SchemaUpdate(configuration);

            update.Execute(false, true);

            if (update.Exceptions.Count > 0)
                throw new ArgumentException("Invalid Migrations", update.Exceptions[0].Message);
        }

    }
}
