﻿using Box.Data.NHibernate.UnitOfWork;
namespace BoxApp.Data.NHibernate
{
    public class BoxExampleUnitOfWork : ANHUnitOfWork
    {
        public BoxExampleUnitOfWork()
        {
            this.instance = BoxExampleSessionFactory.Create();
        }
    }
}
