﻿using Box.Data.Core;
using Box.Data.NHibernate.Repository;
using BoxExample.NameModule.Domain;
using BoxExample.NameModule.Domain.Municipios;

namespace BoxApp.NameModule.Data.Repositories
{
    public class MunicipioRepository : HibernateRepository<Municipio>, IMunicipioRepository
    {
        public MunicipioRepository(IQueryableUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
