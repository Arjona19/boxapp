﻿using BoxExample.NameModule.Domain;
using BoxExample.NameModule.Domain.Estados;
using Box.Data.Core;
using Box.Data.NHibernate.Repository;

namespace BoxApp.NameModule.Data.Repositories
{
    public class EstadoRepository : HibernateRepository<Estado>, IEstadoRepository
    {
        public EstadoRepository(IQueryableUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
