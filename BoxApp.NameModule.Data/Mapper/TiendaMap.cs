﻿using BoxExample.NameModule.Domain;
using FluentNHibernate.Mapping;

namespace CREDITOS.Base.Data.Mapper
{
    public class TiendaMap : ClassMap<Tienda>
    {
        public TiendaMap()
        {
            this.Table("Tienda");
            this.DynamicUpdate();
            this.DynamicInsert();

            this.Id(x => x.TiendaID)
              .Column("TiendaID")
              .Not.Nullable()
              .GeneratedBy.GuidComb();

            this.Map(x => x.RFC)
                .Column("RFC")
                .Length(13)
                .Not.Nullable();

            this.Map(x => x.RazonSocial)
                .Column("RazonSocial")
                .Length(100)
                .Not.Nullable();

            this.Map(x => x.NombreComercial)
                .Column("NombreComercial")
                .Length(100);

            this.Map(x => x.Email)
                .Column("Email")
                .Length(100);

            this.Map(x => x.Telefono)
                .Column("Telefono")
                .Length(50);

            this.HasMany(x => x.TiendaDomicilios)
                .LazyLoad()
                .Inverse()
                .Cascade.None()
                .ReadOnly()
                .KeyColumn("TiendaID");
        }
    }
}
