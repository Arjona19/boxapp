﻿using BoxExample.NameModule.Domain;
using FluentNHibernate.Mapping;

namespace CREDITOS.Base.Data.Mapper
{
    public class TiendaDomicilioMap : SubclassMap<TiendaDomicilio>
    {
        public TiendaDomicilioMap()
        {
            this.Table("TiendaDomicilio");

            this.Map(x => x.Nombre)
                .Column("Nombre")
                .Length(100)
                .Not.Nullable();

            this.Map(x => x.EstaActivo)
                .Column("EstaActivo")
                .Not.Nullable();

            this.Map(x => x.TipoTienda)
                .CustomType(typeof(ETIpoTienda?))
                .Column("TipoTienda")
                .Not.Nullable();

            this.References(x => x.Tienda)
                .LazyLoad()
                .Fetch.Join()
                .Column("TiendaID")
                .ForeignKey("TiendaDomicilioToTienda")
                .Not.Nullable()
                .Cascade.None()
                .Not.Insert()
                .Not.Update()
                .ReadOnly();

            this.Map(x => x.TiendaID)
                .Column("TiendaID")
                .Not.Nullable();
        }
    }
}
