﻿using BoxExample.NameModule.Domain;
using FluentNHibernate.Mapping;

namespace CREDITOS.Base.Data.Mapper
{
    public class DomicilioMap : ClassMap<ADomicilio>
    {
        public DomicilioMap()
        {
            //Habilitar la herencia de clases abstractas y subclases en el mapeo.
            this.UseUnionSubclassForInheritanceMapping();

            this.DynamicUpdate();
            this.DynamicInsert();

            this.Id(x => x.DomicilioID)
                .Column("DomicilioID")
                .Not.Nullable()
                .GeneratedBy.GuidComb();
            this.Map(x => x.Direccion)
                .Column("Direccion")
                .Not.Nullable()
                .Length(120);
            this.Map(x => x.Colonia)
                .Column("Colonia")
                .Not.Nullable()
                .Length(35);
            this.Map(x => x.CP)
                .Column("CP")
                .Not.Nullable();
            this.Map(x => x.NumExt)
                .Column("NumExt")
                .Not.Nullable()
                .Length(10);
            this.Map(x => x.NumInt)
                .Column("NumInt")
                .Nullable()
                .Length(10);
            this.Map(x => x.Telefono)
                .Column("Telefono")
                .Nullable()
                .Length(25);

            this.References(x => x.Estado)
                .LazyLoad()
                .Fetch.Join()
                .Column("EstadoID")
                .ForeignKey("DomicilioToEstado")
                .Not.Nullable()
                .Cascade.None()
                .Not.Insert()
                .Not.Update()
                .ReadOnly();
            this.References(x => x.Municipio)
                .LazyLoad()
                .Fetch.Join()
                .Column("MunicipioID")
                .ForeignKey("DomicilioToMunicipio")
                .Not.Nullable()
                .Cascade.None()
                .Not.Insert()
                .Not.Update()
                .ReadOnly();
            this.References(x => x.Localidad)
                .LazyLoad()
                .Fetch.Join()
                .Column("LocalidadID")
                .ForeignKey("DomicilioToLocalidad")
                .Not.Nullable()
                .Cascade.None()
                .Not.Insert()
                .Not.Update()
                .ReadOnly();

            this.Map(x => x.EstadoID)
                .Column("EstadoID")
                .Not.Nullable();
            this.Map(x => x.MunicipioID)
                .Column("MunicipioID")
                .Not.Nullable();
            this.Map(x => x.LocalidadID)
                .Column("LocalidadID")
                .Not.Nullable();
        }
    }
}
