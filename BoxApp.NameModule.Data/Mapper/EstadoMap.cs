﻿using BoxExample.NameModule.Domain;
using FluentNHibernate.Mapping;

namespace BoxApp.NameModule.Data.Mapper
{
    public class EstadoMap : ClassMap<Estado>
    {
        public EstadoMap()
        {
            this.Table("Estado");

            this.Id(x => x.EstadoID)
                .GeneratedBy.Increment()
                .Column("EstadoID")
                .Not.Nullable();

            this.Map(x => x.Nombre)
                .Column("Nombre")
                .Length(50)
                .Nullable();
        }
    }
}
