﻿using BoxExample.NameModule.Domain;
using FluentNHibernate.Mapping;

namespace CREDITOS.Base.Data.Mapper
{
    public class LocalidadMap : ClassMap<Localidad>
    {
        public LocalidadMap()
        {
            this.Table("Localidad");
            this.DynamicUpdate();
            this.DynamicInsert();
            
            this.Id(x => x.LocalidadID)
                .Column("LocalidadID")
                .Not.Nullable()
                .GeneratedBy.Identity();

            this.Map(x => x.Nombre)
                .Column("Nombre")
                .Length(50)
                .Not.Nullable();

            this.References(x => x.Municipio)
                .LazyLoad()
                .Fetch.Join()
                .Column("MunicipioID")
                .ForeignKey("LocalidadToMunicipio")
                .Not.Nullable()
                .Cascade.None()
                .Not.Insert()
                .Not.Update()
                .ReadOnly();
            
            this.Map(x => x.MunicipioID)
                .Column("MunicipioID")
                .Not.Nullable();
        }
    }
}