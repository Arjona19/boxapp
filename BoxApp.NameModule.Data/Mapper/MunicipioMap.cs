﻿using BoxExample.NameModule.Domain;
using FluentNHibernate.Mapping;

namespace CREDITOS.Base.Data.Mapper
{
    public class MunicipioMap : ClassMap<Municipio>
    {
        public MunicipioMap()
        {
            this.Table("Municipio");
            this.DynamicUpdate();
            this.DynamicInsert();
            
            this.Id(x => x.MunicipioID)
                .Column("MunicipioID")
                .Not.Nullable()
                .GeneratedBy.Identity();

            this.Map(x => x.Nombre)
                .Column("Nombre")
                .Length(50)
                .Not.Nullable();

            this.References(x => x.Estado)
                .LazyLoad()
                .Fetch.Join()
                .Column("EstadoID")
                .ForeignKey("MunicipioToEstado")
                .Not.Nullable()
                .Cascade.None()
                .Not.Insert()
                .Not.Update()
                .ReadOnly();
            
            this.Map(x => x.EstadoID)
                .Column("EstadoID")
                .Not.Nullable();
        }
    }
}