﻿using System;
using System.Linq.Expressions;

namespace Box.Specification.LTE
{
    public sealed class TrueSpecification<TEntity> : Specification<TEntity>
   where TEntity : class
    {
        public TrueSpecification()
        {
        }

        public override Expression<Func<TEntity, bool>> SatisfiedBy()
        {
            bool flag = true;
            return (TEntity t) => flag;
        }
    }
}
