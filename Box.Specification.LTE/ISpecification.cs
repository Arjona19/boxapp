﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Box.Specification.LTE
{
    public interface ISpecification<TEntity>
      where TEntity : class
    {
        Expression<Func<TEntity, bool>> SatisfiedBy();
    }
}
