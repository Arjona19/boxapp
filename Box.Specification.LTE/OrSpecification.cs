﻿using System;
using System.Linq.Expressions;

namespace Box.Specification.LTE
{
    public sealed class OrSpecification<T> : CompositeSpecification<T>
     where T : class
    {
        private ISpecification<T> _RightSideSpecification;

        private ISpecification<T> _LeftSideSpecification;

        public override ISpecification<T> LeftSideSpecification
        {
            get
            {
                return this._LeftSideSpecification;
            }
        }

        public override ISpecification<T> RightSideSpecification
        {
            get
            {
                return this._RightSideSpecification;
            }
        }

        public OrSpecification(ISpecification<T> leftSide, ISpecification<T> rightSide)
        {
            if (leftSide == null)
            {
                throw new ArgumentNullException("leftSide");
            }
            if (rightSide == null)
            {
                throw new ArgumentNullException("rightSide");
            }
            this._LeftSideSpecification = leftSide;
            this._RightSideSpecification = rightSide;
        }

        public override Expression<Func<T, bool>> SatisfiedBy()
        {
            return this._LeftSideSpecification.SatisfiedBy().Or<T>(this._RightSideSpecification.SatisfiedBy());
        }
    }
}
