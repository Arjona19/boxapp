﻿using System;
using System.Linq;
using System.Linq.Expressions;
namespace Box.Specification.LTE
{
    public static class ExpressionBuilder
    {
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose<Func<T, bool>>(second, new Func<Expression, Expression, Expression>(Expression.AndAlso));
        }

        public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
        {
            Expression expression = ParameterRebinder.ReplaceParameters(first.Parameters.Select((ParameterExpression f, int i) => new { f = f, s = second.Parameters[i] }).ToDictionary((p) => p.s, (p) => p.f), second.Body);
            return Expression.Lambda<T>(merge(first.Body, expression), first.Parameters);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        {
            return first.Compose<Func<T, bool>>(second, new Func<Expression, Expression, Expression>(Expression.OrElse));
        }
    }
}
