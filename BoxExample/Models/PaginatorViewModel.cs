﻿using System.Collections.Generic;

namespace BoxExample.Models
{
    public class PaginatorViewModel<T> where T : class
    {
        public long TotalPages { get; set; }
        public long CurrentPage { get; set; }
        public List<T> Items { get; set; }
    }
}