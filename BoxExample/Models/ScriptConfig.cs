﻿using System.IO;
using System.Web;
using System.Web.Mvc;

namespace BoxExample.Models
{
    public static class ScriptConfig
    {
        public static string LastModified(string file)
        {
            return File.GetLastWriteTime(file).ToString("yyyyMMddTHHmmss");
        }

        public static string ContentWithVersion(this UrlHelper helper, string file)
        {
            var server = HttpContext.Current.Server;

            return helper.Content(file + "?v=" + File.GetLastWriteTime(server.MapPath(file)).ToString("yyyyMMddTHHmmss"));
        }
    }
}