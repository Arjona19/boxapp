﻿(function () {
    'use strict';

    angular
        .module('estadosApp')
        .controller('estadoNewController', estadoNewController);

    //////////////////////////////

    estadoNewController.$inject = ['$rootScope', '$q', '$location', '$log', '$timeout', 'filterFilter', 'toaster', 'estadosData'];

    function estadoNewController($rootScope, $q, $location, $log, $timeout, filterFilter, toaster, estadosData) {
        var vm = this;

        vm.estado = {};

        vm.save = save;
        vm.cancel = cancel;

        vm.isDelete = false;
        vm.success = false;

        //////////////////////////////

        function save() {
            vm.toastBlock = toaster.pop({ type: 'wait', title: 'Estado', body: 'Procesando solicitud, espere...', toasterId: 0 });

            var estado = angular.copy(vm.estado);

            return saveEstado(estado)
                .then(function () {
                    toaster.pop({ type: 'info', title: 'Estado', body: 'Guardado exitoso!', toasterId: 1 });

                    if (vm.success)
                        $timeout(function () { $location.path('/') }, 2000);
                })
                .catch(function (data) {
                    toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                })
                .finally(function () {
                    toaster.clear(vm.toastBlock);
                });
        }

        function saveEstado(estado) {
            return estadosData.save(estado)
                .then(function (data) {
                    vm.success = true;

                    return data;
                })
                .catch(function (data) {

                    return $q.reject(data);
                });

        }

        function cancel() {
            $location.path('/');
        }
    }
})();