﻿(function () {
    'use strict';

    angular
        .module('estadosApp')
        .controller('estadoEditController', estadoEditController);

    //////////////////////////////

    estadoEditController.$inject = ['$q', '$routeParams', '$location', '$log', '$timeout', 'filterFilter', 'jsDateFilter', 'toaster', 'estadosData'];

    function estadoEditController($q, $routeParams, $location, $log, $timeout, filterFilter, jsDateFilter, toaster, estadosData) {
        var vm = this;

        vm.estado = undefined;

        vm.save = save;
        vm.cancel = cancel;

        vm.isDelete = false;
        vm.success = false;

        activate();

        //////////////////////////////

        function activate() {
            vm.toastBlock = toaster.pop({ type: 'wait', title: 'Estado', body: 'Procesando solicitud, espere...', toasterId: 0 });

            return getEstado($routeParams.estadoid)
                .then(function () {
                    toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                })
                .catch(function (data) {
                    toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                })
                .finally(function () {
                    toaster.clear(vm.toastBlock);
                });
        }

        function getEstado(estadoid) {
            return estadosData.get(estadoid)
                .then(function (data) {
                    vm.estado = data;

                    return data;
                })
                .catch(function (data) {

                    return $q.reject(data);
                });
        }

        function save() {
            vm.toastBlock = toaster.pop({ type: 'wait', title: 'Estado', body: 'Procesando solicitud, espere...', toasterId: 0 });

            var estado = angular.copy(vm.estado);

            return saveEstado(estado)
                .then(function () {
                    toaster.pop({ type: 'success', title: 'Estado', body: 'Guardado exitoso!', toasterId: 1 });

                    if (vm.success)
                        $timeout(function () { $location.path('/') }, 2000);
                })
                .catch(function (data) {
                    toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                })
                .finally(function () {
                    toaster.clear(vm.toastBlock);
                });
        }

        function saveEstado(estado) {
            return estadosData.save(estado)
                .then(function (data) {
                    vm.success = true;

                    return data;
                })
                .catch(function (data) {

                    return $q.reject(data);
                });
        }

        function cancel() {
            $location.path('/');
        }
    }
})();
