﻿(function () {
    'use strict';

    angular
        .module('estadosApp')
        .factory('estadosData', estadosData);

    //////////////////////////////

    estadosData.$inject = ['$http', '$q', '$log'];

    function estadosData($http, $q, $log) {
        var service = {
            getPage: getPage,
            get: get,
            save: save,
            remove: remove
        };

        return service;

        //////////////////////////////

        /* getPage */

        function getPage(page, size, sortBy, reverse, search) {
            return $http({ method: 'GET', url: 'load/', params: { page: page, items: size, sortBy: sortBy, asc: !reverse, filter: search } })
                .then(complete)
                .catch(failed);
        }

        /* get */

        function get(id) {
            return $http({ method: 'GET', url: 'find/', params: { id: id } })
                .then(complete)
                .catch(failed);
        }

        /* save */

        function save(estado) {
            return $http({ method: 'POST', url: 'save/', data: { model: estado } })
                .then(complete)
                .catch(failed);
        }

        /* remove */
        function remove(id) {
            return $http({ method: 'POST', url: 'remove/', params: { id: id } })
                .then(complete)
                .catch(failed);
        }

        function complete(response) {
            $log.info(response.data, response.status, response.headers(), response.config);

            return response.data;
        }

        function failed(error) {
            error.data = error.data || { Error: 'Desconocido', Message: 'Desconocido' };

            $log.warn(error.data, error.status, error.headers(), error.config);

            return $q.reject(error.data);
        }
    }
})();