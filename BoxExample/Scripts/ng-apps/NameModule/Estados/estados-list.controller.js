﻿(function () {
    'use strict';

    angular
        .module('estadosApp')
        .controller('estadoListController', estadoListController);

    //////////////////////////////

    estadoListController.$inject = ['$rootScope', '$q', '$location', '$locale', '$log', 'filterFilter', 'toaster', 'mostrarElementos', 'estadosData'];

    function estadoListController($rootScope, $q, $location, $locale, $log, filterFilter, toaster, mostrarElementos, estadosData) {
        var vm = this;

        vm.estadoid = undefined;
        vm.estados = [];
        vm.mostrarElementos = mostrarElementos;
        vm.setSelected = setSelected;
        vm.pagingInfoEstado = {
            rows: 0,
            total: 0,
            current: 0,
            pages: [],
            size: 10,
            filtered: false,
            search: undefined,
            sortBy: 'Nombre',
            reverse: false
        };

        vm.navigation = {
            search: function () {
                vm.estadoid = undefined;
                vm.pagingInfoEstado.current = 0;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });

                        vm.pagingInfoEstado.filtered = true;
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            clear: function () {
                vm.estadoid = undefined;
                vm.pagingInfoEstado.search = undefined;
                vm.pagingInfoEstado.current = 0;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });

                        vm.pagingInfoEstado.filtered = false;
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            sort: function (sortBy) {
                vm.estadoid = undefined;
                vm.pagingInfoEstado.current = 0;

                if (sortBy === vm.pagingInfoEstado.sortBy) {
                    vm.pagingInfoEstado.reverse = !vm.pagingInfoEstado.reverse;
                } else {
                    vm.pagingInfoEstado.sortBy = sortBy;
                    vm.pagingInfoEstado.reverse = false;
                }

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            size: function () {
                vm.estadoid = undefined;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            first: function () {
                vm.estadoid = undefined;
                vm.pagingInfoEstado.current = 0;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            next: function () {
                vm.estadoid = undefined;
                vm.pagingInfoEstado.current = vm.pagingInfoEstado.current + 1;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            current: function () {
                vm.estadoid = undefined;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            page: function () {
                vm.estadoid = undefined;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            prior: function () {
                vm.estadoid = undefined;
                vm.pagingInfoEstado.current = vm.pagingInfoEstado.current - 1;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            },
            last: function () {
                vm.estadoid = undefined;
                vm.pagingInfoEstado.current = vm.pagingInfoEstado.total - 1;

                return getEstados()
                    .then(function () {
                        toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                    })
                    .catch(function (data) {
                        toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                    });
            }
        };

        activate();

        //////////////////////////////

        function activate() {
            return getEstados()
                .then(function () {
                    toaster.pop({ type: 'success', title: 'Estado', body: 'Recuperación exitosa!', toasterId: 1 });
                })
                .catch(function (data) {
                    toaster.pop({ type: 'error', title: 'Estado', body: data.Message, toasterId: 1 });
                });
        }

        function getEstados() {
            vm.toastBlock = toaster.pop({ type: 'wait', title: 'Estado', body: 'Procesando solicitud, espere...', toasterId: 0 });

            return estadosData.getPage(vm.pagingInfoEstado.current, vm.pagingInfoEstado.size, vm.pagingInfoEstado.sortBy, vm.pagingInfoEstado.reverse, vm.pagingInfoEstado.search)
                .then(function (data) {
                    vm.estados = data.Items;

                    vm.pagingInfoEstado.rows = data.TotalRows;
                    vm.pagingInfoEstado.total = data.TotalPages;
                    vm.pagingInfoEstado.current = data.CurrentPage;
                    vm.pagingInfoEstado.pages = [];

                    for (var i = 0; i < data.TotalPages; i++) {
                        vm.pagingInfoEstado.pages.push({ "name": (i + 1).toString(), "value": i });
                    }

                    return vm.estados;
                })
                .catch(function (data) {
                    vm.estados = [];

                    return $q.reject(data);
                })
                .finally(function () {
                    toaster.clear(vm.toastBlock);
                });
        }

        function setSelected(id) {
            if (vm.notSelect)
                return;

            if (id === vm.estadoid) {
                vm.estadoid = undefined;
            } else {
                vm.estadoid = id;
            }
        }
    }
})();