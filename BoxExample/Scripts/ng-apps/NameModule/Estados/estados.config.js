﻿(function () {
    'use strict';

    angular
        .module('estadosApp')
        .config(config);

    function config($routeProvider) {
        $routeProvider
            .when('/delete/:estadoid', {
                controller: 'estadoDeleteController',
                controllerAs: 'vm',
                templateUrl: 'estado/edit'
            })
            .when('/edit/:estadoid', {
                controller: 'estadoEditController',
                controllerAs: 'vm',
                templateUrl: 'estado/edit'
            })
            .when('/new', {
                controller: 'estadoNewController',
                controllerAs: 'vm',
                templateUrl: 'estado/edit'
            })
            .when('/', {
                controller: 'estadoListController',
                controllerAs: 'vm',
                templateUrl: 'estado/list'
            })
            .otherwise({ redirectTo: '/' });
    }

    //////////////////////////////

})();