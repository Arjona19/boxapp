﻿(function () {
    'use strict';

    angular
        .module('estadosApp', [
            'ngSanitize',
            'ngResource',
            'ngRoute',
            'ngCookies',
            'ngAnimate',
            'toaster',
            'ui.toggle',
            'coreApp'
        ]);
})();