﻿(function () {
    'use strict';

    angular
        .module('coreApp')
        .filter("jsDate", jsDateFilter)
        .filter('startFrom', startFromFilter)
        .filter('siNo', siNoFilter);

    //////////////////////////////

    function jsDateFilter() {
        return function (value) {
            return new Date(parseInt(value.substr(6)));
        };
    }

    function startFromFilter() {
        return function (input, start) {
            if (input == undefined)
                return undefined;

            start = +start;
            return input.slice(start);
        }
    }

    function siNoFilter() {
        return function (value) {
            switch (value) {
            case false:
                return "No";
            case true:
                return "Sí";
            default:
                return "No definido";
            }
        };
    }
})();