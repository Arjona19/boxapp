angular
    .module('coreApp')
    .directive('ngNumMin', ngNumMin)
    .directive('ngNumMax', ngNumMax);

//////////////////////////////

function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}

function ngNumMin() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngNumMin, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            
            var minValidator = function (value) {
                var min = scope.$eval(attr.ngNumMin) || 0;
                if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('nummin', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('nummin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
}

function ngNumMax() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngNumMax, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var maxValidator = function (value) {
                var max = scope.$eval(attr.ngNumMax) || Infinity;
                if (!isEmpty(value) && value > max) {
                    ctrl.$setValidity('nummax', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('nummax', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
}