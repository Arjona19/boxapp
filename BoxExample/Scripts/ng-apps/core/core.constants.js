﻿(function () {
    'use strict';

    angular
        .module('coreApp')
        .constant('mostrarElementos', [
            { "name": "5", "value": 5 },
            { "name": "10", "value": 10 },
            { "name": "20", "value": 20 },
            { "name": "50", "value": 50 },
            { "name": "100", "value": 100 }
        ])
        .constant('emailPattern', /^[a-zA-Z0-9.!#$%&'*+\/=?\^_`{\|}~\-]+@[a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?)*$/)
        .constant('rfcPattern', /^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/)
        .constant('dateShortFormat', 'dd-MM-yyyy')
        .constant('dateTimeFormat', 'dd-MM-yyyy hh:mm a');
})();