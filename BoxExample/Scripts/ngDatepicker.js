(function() {
    'use strict';
    (angular.module('ngDatepicker', ['ng'])).directive('ngDatepicker', ['$filter',
        function ($filter) {
            return {
                restrict: 'E',
                replace: true,
                template: function() {
                    return '<input type="text">';
                },
                require: 'ngModel',
                scope: {},
                link: function (scope, element, attrs, ngModel) {

                    ngModel.$parsers.push(function (value) {
                        //convert data from view format to model format
                        return value ? angular.element.datepicker.parseDate(attrs.parse, value) : undefined;
                    });

                    ngModel.$formatters.push(function (data) {
                        //convert data from model format to view format
                        return $filter('date')(new Date(data), attrs.filter);
                    });
 
                    var config = {};

                    if (attrs.min) {
                        scope.readonly = true;
                        config.minDate = attrs.min;
                    }
                    if(attrs.max){
                        scope.readonly = true;
                        config.maxDate = attrs.max;
                    }
                    if (attrs.language) {
                        config.language = attrs.language;
                    }
                    if (attrs.format) {
                        config.format = attrs.format;
                    }

                    config.todayBtn = "linked";
                    config.disableTouchKeyboard = true;
                    config.todayHighlight = true;
                    config.toggleActive = true;
                    config.autoclose = true;
                    
                    element.datepicker(config);

                    scope.$watch(function () {
                            return ngModel.$modelValue;
                        },
                        function (newValue, oldValue) {
                            var pickerDate = !isNaN(element.datepicker('getDate')) ? element.datepicker('getDate') : undefined;
                            //if (newValue) {
                            //if (newValue !== ngModel.$modelValue) {
                            if (oldValue !== newValue) {
                                element.datepicker('setDate', newValue || oldValue);
                            }
                            //}
                            //} else if (oldValue) {
                                //element.datepicker('setDate', oldValue);
                            //}

                            //ngModel.$viewValue = newValue;
                        });
                }
            };
        }
    ]);
})(window, document);