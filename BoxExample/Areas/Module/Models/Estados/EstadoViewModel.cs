﻿
namespace BoxExample.Areas.Module.Models.Estados
{
    public class EstadoViewModel
    {
        public virtual int? EstadoID { get; set; }
        public virtual string Nombre { get; set; }
    }
}
