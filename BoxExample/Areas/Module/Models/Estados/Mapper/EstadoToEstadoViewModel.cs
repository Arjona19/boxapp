﻿using Box.CrossCutting.Mapper.AutoMapper;
using BoxExample.NameModule.Domain;

namespace BoxExample.Areas.Module.Models.Estados.Mapper
{
    public interface IEstadoToEstadoViewModel
    {
        EstadoViewModel Convert(Estado source);
        Estado Convert(EstadoViewModel source);
    }

    public class EstadoToEstadoViewModel : DTOMapper<Estado, EstadoViewModel>, IEstadoToEstadoViewModel
    {
        protected override void SourceToDestinationMap()
        {
            config.CreateMap<Estado, EstadoViewModel>();
        }

        protected override void DestinationToSourceMap()
        {
            config.CreateMap<EstadoViewModel, Estado>();
        }
    }
}
