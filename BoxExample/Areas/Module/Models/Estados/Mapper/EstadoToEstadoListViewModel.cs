﻿using Box.CrossCutting.Mapper.AutoMapper;
using BoxExample.NameModule.Domain;
using System.Collections.Generic;

namespace BoxExample.Areas.Module.Models.Estados.Mapper
{
    public interface IEstadoToEstadoItemListViewModel
    {
        List<EstadoItemListViewModel> Convert(List<Estado> source);
    }

    public class EstadoToEstadoItemListViewModel : DTOMapper<Estado, EstadoItemListViewModel>, IEstadoToEstadoItemListViewModel
    {
    }
}
