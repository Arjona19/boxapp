﻿using Box.CrossCutting.Mapper.AutoMapper;
using BoxExample.NameModule.Domain;

namespace BoxExample.Areas.Module.Models.Municipios.Mapper
{
    public interface IMunicipioToMunicipioViewModel
    {
        MunicipioViewModel Convert(Municipio source);
        Municipio Convert(MunicipioViewModel source);
    }

    public class MunicipioToMunicipioViewModel : DTOMapper<Municipio, MunicipioViewModel>, IMunicipioToMunicipioViewModel
    {
        protected override void SourceToDestinationMap()
        {
            config.CreateMap<Municipio, MunicipioViewModel>();
        }

        protected override void DestinationToSourceMap()
        {
            config.CreateMap<MunicipioViewModel, Municipio>();
        }
    }
}
