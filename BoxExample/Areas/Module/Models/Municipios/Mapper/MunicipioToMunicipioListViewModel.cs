﻿using Box.CrossCutting.Mapper.AutoMapper;
using BoxExample.NameModule.Domain;
using System.Collections.Generic;

namespace BoxExample.Areas.Module.Models.Municipios.Mapper
{
    public interface IMunicipioToMunicipioItemListViewModel
    {
        List<MunicipioItemListViewModel> Convert(List<Municipio> source);
    }

    public class MunicipioToMunicipioItemListViewModel : DTOMapper<Municipio, MunicipioItemListViewModel>, IMunicipioToMunicipioItemListViewModel
    {
    }
}
