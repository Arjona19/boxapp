﻿
namespace BoxExample.Areas.Module.Models.Municipios
{
    public class MunicipioItemListViewModel
    {
        public virtual int? MunicipioID { get; set; }
        public virtual string Nombre { get; set; }
    }
}
