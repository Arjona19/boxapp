﻿using Box.CrossCutting.IoC;
using BoxExample.Areas.Module.Models.Municipios.Mapper;
using BoxExample.CrossCutting.IoC;
using BoxExample.NameModule.Aplication.MunicipioManagement;
using System.Web.Mvc;

namespace BoxExample.Areas.Module.Controllers
{
    public class MunicipioController : Controller
    {
        private static IContainer _container;
        private static IMunicipioManagementServices _app;
        private static IMunicipioToMunicipioItemListViewModel _mapList;
        private static IMunicipioToMunicipioViewModel _map;

        public IContainer Container
        {
            get
            {
                if (_container != null)
                    return _container;

                _container = IoCFactory.Instance.WebContainer;
                _container.RegisterTypeSingleton(typeof(IMunicipioToMunicipioItemListViewModel), typeof(MunicipioToMunicipioItemListViewModel));
                _container.RegisterTypeSingleton(typeof(IMunicipioToMunicipioViewModel), typeof(MunicipioToMunicipioViewModel));

                return _container;
            }
        }

        public IMunicipioToMunicipioItemListViewModel MapList
        {
            get
            {
                if (_mapList != null)
                    return _mapList;

                _mapList = Container.Resolve<IMunicipioToMunicipioItemListViewModel>();

                return _mapList;
            }
        }

        public IMunicipioToMunicipioViewModel Map
        {
            get
            {
                if (_map != null)
                    return _map;

                _map = Container.Resolve<IMunicipioToMunicipioViewModel>();

                return _map;
            }
        }

        public IMunicipioManagementServices App
        {
            get
            {
                if (_app != null)
                    return _app;

                _app = this.Container.Resolve<IMunicipioManagementServices>();

                return _app;
            }
        }
        // GET: Module/Municipio
        public ActionResult Index()
        {
            return View();
        }
    }
}