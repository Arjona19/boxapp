﻿using System.Web.Mvc;
using Box.CrossCutting.IoC;
using BoxExample.CrossCutting.IoC;
using BoxExample.Areas.Module.Models.Estados.Mapper;
using BoxExample.NameModule.Aplication.EstadoManagement;
using System.Linq.Expressions;
using System;
using BoxExample.NameModule.Domain;
using BoxExample.Models;
using System.Net;
using BoxExample.Filters;
using BoxExample.Areas.Module.Models.Estados;

namespace BoxExample.Areas.Module.Controllers
{
    public class EstadoController : Controller
    {
        private static IContainer _container;
        private static IEstadoManagementServices _app;
        private static IEstadoToEstadoItemListViewModel _mapList;
        private static IEstadoToEstadoViewModel _map;

        public IContainer Container
        {
            get
            {
                if (_container != null)
                    return _container;

                _container = IoCFactory.Instance.WebContainer;
                _container.RegisterTypeSingleton(typeof(IEstadoToEstadoItemListViewModel), typeof(EstadoToEstadoItemListViewModel));
                _container.RegisterTypeSingleton(typeof(IEstadoToEstadoViewModel), typeof(EstadoToEstadoViewModel));

                return _container;
            }
        }

        public IEstadoToEstadoItemListViewModel MapList
        {
            get
            {
                if (_mapList != null)
                    return _mapList;

                _mapList = Container.Resolve<IEstadoToEstadoItemListViewModel>();

                return _mapList;
            }
        }

        public IEstadoToEstadoViewModel Map
        {
            get
            {
                if (_map != null)
                    return _map;

                _map = Container.Resolve<IEstadoToEstadoViewModel>();

                return _map;
            }
        }

        public IEstadoManagementServices App
        {
            get
            {
                if (_app != null)
                    return _app;

                _app = this.Container.Resolve<IEstadoManagementServices>();

                return _app;
            }
        }
        // GET: Module/App
        public ActionResult Index()
        {
            return View();
        }

        [AllowJsonGet]
        public ActionResult Load(int page = 0, int items = 10, string sortBy = "Nombre", bool asc = true, string filter = "")
        {
            try
            {
                Expression<Func<Estado, bool>> search = null;
                Expression<Func<Estado, dynamic>> order = null;

                if (filter.Trim().Length > 0)
                {
                    search = x => x.Nombre.Contains(filter);
                }
                else search = x => true;

                var totalItems = this.App.Count(search);

                if (sortBy == "Nombre" || order == null)
                    order = x => x.Nombre;

                var results = this.App.Paged(search, order, page, items, asc);
                var models = this.MapList.Convert(results);

                var paginator = new PaginatorViewModel<EstadoItemListViewModel>();
                paginator.TotalPages = decimal.ToInt64(decimal.Floor(totalItems / items)) + (totalItems % items > 0 ? 1 : 0);
                paginator.CurrentPage = page;
                paginator.Items = models;

                return this.Json(paginator);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new { Error = ex.GetType().ToString(), Message = ex.GetBaseException().Message });
            }
        }

        // GET: Base/Estado/Find/{id}
        [AllowJsonGet]
        public ActionResult Find(int id)
        {
            try
            {
                var result = this.App.FindByIdentifier(id);

                var model = this.Map.Convert(result);

                return this.Json(model);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new { Error = ex.GetType().ToString(), Message = ex.GetBaseException().Message });
            }
        }

        // POST: Base/Estado/Save
        [HttpPost]
        public ActionResult Save(EstadoViewModel model)
        {
            try
            {
                var domain = this.Map.Convert(model);

                if (model.EstadoID == null)
                {
                    this.App.Add(domain);
                }
                else
                {
                    this.App.Update(domain);
                }

                return new EmptyResult();
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new { Error = ex.GetType().ToString(), Message = ex.GetBaseException().Message });
            }
        }

        // POST: Base/Estado/Remove/{id}
        [HttpPost]
        public ActionResult Remove(int id)
        {
            try
            {
                this.App.Remove(id);
            }
            catch (Exception ex)
            {
                this.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return this.Json(new { Error = ex.GetType().ToString(), Message = ex.GetBaseException().Message });
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
