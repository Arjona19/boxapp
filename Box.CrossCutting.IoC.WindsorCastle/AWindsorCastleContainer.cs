﻿using Castle.Windsor;
using Castle.MicroKernel.Registration;
using System;

namespace Box.CrossCutting.IoC.WindsorCastle
{
    public abstract class AWindsorCastleContainer : IContainer, IDisposable
    {
        protected IWindsorContainer container;

        protected AWindsorCastleContainer()
        {
            this.container = (IWindsorContainer)new WindsorContainer();
            this.ConfigureContainer();
        }

        protected abstract void ConfigureContainer();

        public TService Resolve<TService>() => this.container.Resolve<TService>();

        public TService Resolve<TService>(object argumentsConstructor) => this.container.Resolve<TService>(argumentsConstructor);

        public object Resolve(Type type) => this.container.Resolve(type);

        public object Resolve(Type type, object argumentsConstructor) => this.container.Resolve(type, argumentsConstructor);

        public void RegisterType<TImpl>(params Action<TImpl>[] actions) where TImpl : class => this.container.Register(new IRegistration[1]
        {
      (IRegistration) Component.For<TImpl>().OnCreate(actions).LifeStyle.Transient
        });

        public void RegisterType(Type type) => this.container.Register(new IRegistration[1]
        {
      (IRegistration) ((ComponentRegistration<object>) Component.For(type)).LifeStyle.Transient
        });

        public void RegisterTypeSingleton<TImpl>(params Action<TImpl>[] actions) where TImpl : class => this.container.Register(new IRegistration[1]
        {
      (IRegistration) Component.For<TImpl>().OnCreate(actions)
        });

        public void RegisterTypeSingleton(Type type) => this.container.Register(new IRegistration[1]
        {
      (IRegistration) Component.For(type)
        });

        public void RegisterType<TDef, TImpl>(params Action<TDef>[] actions)
          where TDef : class
          where TImpl : class, TDef
        {
            this.container.Register(new IRegistration[1]
            {
        (IRegistration) Component.For<TDef>().ImplementedBy<TImpl>().OnCreate(actions).LifeStyle.Transient
            });
        }

        public void RegisterType(Type forType, Type implementedBy) => this.container.Register(new IRegistration[1]
        {
      (IRegistration) ((ComponentRegistration<object>) Component.For(forType)).ImplementedBy(implementedBy).LifeStyle.Transient
        });

        public void RegisterTypeSingleton<TDef, TImpl>(params Action<TDef>[] actions)
          where TDef : class
          where TImpl : class, TDef
        {
            this.container.Register(new IRegistration[1]
            {
        (IRegistration) Component.For<TDef>().ImplementedBy<TImpl>().OnCreate(actions)
            });
        }

        public void RegisterTypeSingleton(Type forType, Type implementedBy) => this.container.Register(new IRegistration[1]
        {
      (IRegistration) ((ComponentRegistration<object>) Component.For(forType)).ImplementedBy(implementedBy)
        });

        public void RegisterTypeWithInstance(Type forType, object instance) => this.container.Register(new IRegistration[1]
        {
      (IRegistration) ((ComponentRegistration<object>) Component.For(forType)).Instance(instance)
        });

        public void Dispose() => ((IDisposable)this.container).Dispose();
    }
}
