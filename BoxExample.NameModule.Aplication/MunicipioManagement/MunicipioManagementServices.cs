﻿using Box.Application.Core;
using Box.Core;
using BoxExample.NameModule.Domain;
using BoxExample.NameModule.Domain.Municipios;
using System.Linq;

namespace BoxExample.NameModule.Aplication.MunicipioManagement
{
    public class MunicipioManagementServices : AEnterpriseManagementServices<Municipio, IRepository<Municipio>, int>, IMunicipioManagementServices
    {
        private IMunicipioRepository _repository = null;

        public MunicipioManagementServices(IMunicipioRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override Municipio FindByIdentifier(int identifier)
        {
            using (var uow = _repository.UnitOfWork.Open())
            {
                return _repository.FilterElements(exp => exp.MunicipioID == identifier).FirstOrDefault();
            }
        }

        public void Remove(int identifier)
        {
            using (var uow = _repository.UnitOfWork.Open())
            {
                var model = _repository.FilterElements(x => x.MunicipioID == identifier).FirstOrDefault();
                _repository.Remove(model);
            }
        }
    }
}
