﻿using BoxExample.NameModule.Domain;
using Box.Application.Core;
using Box.Core;

namespace BoxExample.NameModule.Aplication.MunicipioManagement
{
    public interface IMunicipioManagementServices : IEnterpriseApplication<Municipio, IRepository<Municipio>, int>
    {
    }
}
