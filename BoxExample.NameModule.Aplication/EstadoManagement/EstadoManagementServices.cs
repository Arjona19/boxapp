﻿using Box.Application.Core;
using Box.Core;
using BoxExample.NameModule.Domain;
using BoxExample.NameModule.Domain.Estados;
using System.Linq;

namespace BoxExample.NameModule.Aplication.EstadoManagement
{
    public class EstadoManagementServices : AEnterpriseManagementServices<Estado, IRepository<Estado>, int>, IEstadoManagementServices
    {
        private IEstadoRepository _repository = null;

        public EstadoManagementServices(IEstadoRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public override Estado FindByIdentifier(int identifier)
        {
            using (var uow = _repository.UnitOfWork.Open())
            {
                return _repository.FilterElements(exp => exp.EstadoID == identifier).FirstOrDefault();
            }
        }

        public void Remove(int identifier)
        {
            using (var uow = _repository.UnitOfWork.Open())
            {
                var model = _repository.FilterElements(x => x.EstadoID == identifier).FirstOrDefault();

                _repository.Remove(model);

                uow.Commit();
            }
        }
    }
}
