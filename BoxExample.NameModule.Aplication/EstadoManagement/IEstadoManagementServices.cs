﻿using BoxExample.NameModule.Domain;
using Box.Application.Core;
using Box.Core;

namespace BoxExample.NameModule.Aplication.EstadoManagement
{
    public interface IEstadoManagementServices : IEnterpriseApplication<Estado, IRepository<Estado>, int>
    {
        void Remove(int identifier);
    }
}
