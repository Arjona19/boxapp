﻿using Box.Core;
using Box.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
namespace Box.Application.Core
{
    public class ManagementService<TEntity, TRepository> : IApplication<TEntity, TRepository>
     where TEntity : class
     where TRepository : IRepository<TEntity>
    {
        protected IQueryableUnitOfWork unitOfWork;
        protected TRepository repository;

        protected ManagementService(IQueryableUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.repository = (TRepository)Activator.CreateInstance(typeof(TRepository), (object)this.unitOfWork);
        }

        public virtual void Add(TEntity item)
        {
            using (IUnitOfWork unitOfWork = this.repository.UnitOfWork.Open())
            {
                this.repository.Add(item);
                unitOfWork.Commit();
            }
        }

        public virtual void Update(TEntity item)
        {
            using (IUnitOfWork unitOfWork = this.repository.UnitOfWork.Open())
            {
                this.repository.Update(item);
                unitOfWork.Commit();
            }
        }

        public virtual void Remove(TEntity item)
        {
            using (IUnitOfWork unitOfWork = this.repository.UnitOfWork.Open())
            {
                this.repository.Remove(item);
                unitOfWork.Commit();
            }
        }

        public virtual TEntity FindBy(Expression<Func<TEntity, bool>> filter)
        {
            using (this.repository.UnitOfWork.Open())
                return this.repository.FilterElements(filter).FirstOrDefault<TEntity>();
        }

        public virtual List<TEntity> FindListBy(Expression<Func<TEntity, bool>> filter)
        {
            List<TEntity> entityList = new List<TEntity>();
            using (this.repository.UnitOfWork.Open())
                return this.repository.FilterElements(filter).ToList<TEntity>();
        }

        public virtual long Count(Expression<Func<TEntity, bool>> filter)
        {
            using (this.repository.UnitOfWork.Open())
                return this.repository.Count(filter);
        }

        public virtual List<TEntity> Paged<S>(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, S>> orderByExpression,
          int pageIndex,
          int pageCount,
          bool ascending)
        {
            List<TEntity> entityList = new List<TEntity>();
            using (this.repository.UnitOfWork.Open())
                return this.repository.FilterElements<S>(filter, orderByExpression, pageIndex, pageCount, ascending).ToList<TEntity>();
        }

        public virtual List<TEntity> FindAll()
        {
            List<TEntity> entityList = new List<TEntity>();
            using (this.repository.UnitOfWork.Open())
                return this.repository.All().ToList<TEntity>();
        }
    }
}
