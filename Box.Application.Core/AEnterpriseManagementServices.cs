﻿using Box.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Box.Application.Core
{
    public abstract class AEnterpriseManagementServices<TEntity, TRepository, TIndentifier> :
    IEnterpriseApplication<TEntity, TRepository, TIndentifier>
    where TEntity : class
    where TRepository : IRepository<TEntity>
    where TIndentifier : struct
    {
        private TRepository repository;

        protected AEnterpriseManagementServices(TRepository repository) => this.repository = (object)repository != null ? repository : throw new ArgumentNullException(nameof(repository));

        public virtual void Add(TEntity item)
        {
            if ((object)item == null)
                throw new ArgumentNullException(nameof(item));
            using (IUnitOfWork unitOfWork = this.repository.UnitOfWork.Open())
            {
                this.repository.Add(item);
                unitOfWork.Commit();
            }
        }

        public virtual void Update(TEntity item)
        {
            if ((object)item == null)
                throw new ArgumentNullException(nameof(item));
            using (IUnitOfWork unitOfWork = this.repository.UnitOfWork.Open())
            {
                this.repository.Update(item);
                unitOfWork.Commit();
            }
        }

        public virtual void Remove(TEntity item)
        {
            if ((object)item == null)
                throw new ArgumentNullException(nameof(item));
            using (IUnitOfWork unitOfWork = this.repository.UnitOfWork.Open())
            {
                this.repository.Remove(item);
                unitOfWork.Commit();
            }
        }

        public abstract TEntity FindByIdentifier(TIndentifier identifier);

        public virtual long Count(Expression<Func<TEntity, bool>> filter)
        {
            using (this.repository.UnitOfWork.Open())
                return this.repository.Count(filter);
        }

        public virtual List<TEntity> Paged<S>(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, S>> orderByExpression,
          int pageIndex,
          int pageCount,
          bool ascending)
        {
            List<TEntity> entityList = new List<TEntity>();
            using (this.repository.UnitOfWork.Open())
                return this.repository.FilterElements<S>(filter, orderByExpression, pageIndex, pageCount, ascending).ToList<TEntity>();
        }

        public virtual List<TEntity> FindAll()
        {
            List<TEntity> entityList = new List<TEntity>();
            using (this.repository.UnitOfWork.Open())
                return this.repository.All().ToList<TEntity>();
        }
    }
}
