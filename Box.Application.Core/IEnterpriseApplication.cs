﻿using Box.Core;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Box.Application.Core
{
    public interface IEnterpriseApplication<TEntity, TRepository, in TIndentifier>
    where TEntity : class
    where TRepository : IRepository<TEntity>
    where TIndentifier : struct
    {
        void Add(TEntity item);

        void Update(TEntity item);

        void Remove(TEntity item);

        TEntity FindByIdentifier(TIndentifier identifier);

        long Count(Expression<Func<TEntity, bool>> filter);

        List<TEntity> Paged<S>(
          Expression<Func<TEntity, bool>> filter,
          Expression<Func<TEntity, S>> orderByExpression,
          int pageIndex,
          int pageCount,
          bool ascending);

        List<TEntity> FindAll();
    }
}
