﻿using FluentNHibernate.Mapping;
using Box.Infraestructure.Business.Core.Files.Domain;
using System;
using System.Linq.Expressions;
namespace Box.Infraestructure.Business.Core.Files.Data.Mapper
{
    public class ATipoArchivoMap : ClassMap<ATipoArchivo>
    {
        public ATipoArchivoMap()
        {
            this.UseUnionSubclassForInheritanceMapping();
            this.OptimisticLock.All();
            this.DynamicUpdate();
            this.Id((Expression<Func<ATipoArchivo, object>>)(x => (object)x.TipoArchivoID)).Column("TipoArchivoID").Not.Nullable();
            this.Map((Expression<Func<ATipoArchivo, object>>)(x => x.Descripcion)).Column("Descripcion").Not.Nullable().Length(200);
            this.Map((Expression<Func<ATipoArchivo, object>>)(x => x.Extension)).Column("Extension").Not.Nullable().Length(5);
            this.Map((Expression<Func<ATipoArchivo, object>>)(x => x.MimeType)).Column("MimeType").Not.Nullable().Length(100);
            this.Map((Expression<Func<ATipoArchivo, object>>)(x => (object)x.Activo)).Column("Activo").Not.Nullable();
            this.Map((Expression<Func<ATipoArchivo, object>>)(x => (object)x.EsImagen)).Column("EsImagen").Not.Nullable();
        }
    }
}
