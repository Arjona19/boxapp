﻿using FluentNHibernate.Mapping;
using Box.Infraestructure.Business.Core.Files.Domain;
using System;
using System.Linq.Expressions;

namespace Box.Infraestructure.Business.Core.Files.Data.Mapper
{
    public class AArchivoMap : ClassMap<AArchivo>
    {
        public AArchivoMap()
        {
            this.UseUnionSubclassForInheritanceMapping();
            this.DynamicUpdate();
            this.OptimisticLock.Version();
            this.Version((Expression<Func<AArchivo, object>>)(x => x.Version)).Column("Version").CustomSqlType("timestamp").Generated.Always().UnsavedValue("null");
            this.Id((Expression<Func<AArchivo, object>>)(x => (object)x.ArchivoID)).Column("ArchivoID").Not.Nullable().GeneratedBy.GuidComb();
            this.Map((Expression<Func<AArchivo, object>>)(x => x.Nombre)).Column("Nombre").Nullable().Length(500);
            this.Map((Expression<Func<AArchivo, object>>)(x => x.Ruta)).Column("Ruta").Nullable().Length(2000);
            this.Map((Expression<Func<AArchivo, object>>)(x => x.Contenido)).Column("Contenido").CustomSqlType("varbinary(max)").Length(int.MaxValue).Nullable();
            this.References<ATipoArchivo>((Expression<Func<AArchivo, ATipoArchivo>>)(x => x.TipoArchivo)).LazyLoad().Fetch.Join().Column("TipoArchivoID").Not.Nullable().Cascade.None().Not.Insert().Not.Update().ReadOnly();
            this.Map((Expression<Func<AArchivo, object>>)(x => (object)x.TipoArchivoID)).Column("TipoArchivoID").Not.Nullable();
        }
    }
}
