﻿using FluentNHibernate.Mapping;
using Box.Infraestructure.Business.Core.Files.Domain;
using System;
using System.Linq.Expressions;
namespace Box.Infraestructure.Business.Core.Files.Data.Mapper
{
    public class ADocumentoMap : ClassMap<ADocumento>
    {
        public ADocumentoMap()
        {
            this.UseUnionSubclassForInheritanceMapping();
            this.DynamicUpdate();
            this.OptimisticLock.Version();
            this.Version((Expression<Func<ADocumento, object>>)(x => x.Version)).Column("Version").CustomSqlType("timestamp").Generated.Always().UnsavedValue("null");
            this.Id((Expression<Func<ADocumento, object>>)(x => (object)x.DocumentoID)).Column("DocumentoID").Not.Nullable().GeneratedBy.GuidComb();
            this.Map((Expression<Func<ADocumento, object>>)(x => x.Nombre)).Column("Nombre").Not.Nullable().Length(200);
            this.Map((Expression<Func<ADocumento, object>>)(x => (object)x.Fecha)).Column("Fecha").Not.Nullable();
            this.References<ACarpetaDocumentos>((Expression<Func<ADocumento, ACarpetaDocumentos>>)(x => x.CarpetaDocumentos)).LazyLoad().Fetch.Join().Column("CarpetaDocumentosID").Nullable().Cascade.None().Not.Insert().Not.Update().ReadOnly();
            this.References<AArchivo>((Expression<Func<ADocumento, AArchivo>>)(x => x.Archivo)).LazyLoad().Fetch.Join().Column("ArchivoID").Not.Nullable().Cascade.None().Not.Insert().Not.Update().ReadOnly();
            this.Map((Expression<Func<ADocumento, object>>)(x => (object)x.CarpetaDocumentosID)).Column("CarpetaDocumentosID").Nullable();
            this.Map((Expression<Func<ADocumento, object>>)(x => (object)x.ArchivoID)).Column("ArchivoID").Not.Nullable();
        }
    }
}
