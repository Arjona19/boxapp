﻿using FluentNHibernate.Mapping;
using Box.Infraestructure.Business.Core.Files.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Box.Infraestructure.Business.Core.Files.Data.Mapper
{
    public class ACarpetaDocumentosMap : ClassMap<ACarpetaDocumentos>
    {
        public ACarpetaDocumentosMap()
        {
            this.UseUnionSubclassForInheritanceMapping();
            this.DynamicUpdate();
            this.OptimisticLock.Version();
            this.Version((Expression<Func<ACarpetaDocumentos, object>>)(x => x.Version)).Column("Version").CustomSqlType("timestamp").Generated.Always().UnsavedValue("null");
            this.Id((Expression<Func<ACarpetaDocumentos, object>>)(x => (object)x.CarpetaDocumentosID)).Column("CarpetaDocumentosID").Not.Nullable().GeneratedBy.GuidComb();
            this.Map((Expression<Func<ACarpetaDocumentos, object>>)(x => x.Nombre)).Column("Nombre").Not.Nullable().Length(200);
            this.HasMany<ADocumento>((Expression<Func<ACarpetaDocumentos, IEnumerable<ADocumento>>>)(x => x.Documentos)).LazyLoad().Cascade.None().Inverse().KeyColumn("CarpetaDocumentosID");
            this.Map((Expression<Func<ACarpetaDocumentos, object>>)(x => (object)x.Activo)).Column("Activo").Not.Nullable();
        }
    }
}
