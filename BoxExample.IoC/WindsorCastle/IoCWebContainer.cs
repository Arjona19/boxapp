﻿using Castle.MicroKernel.Registration;
using Box.Core;
using Box.CrossCutting.IoC.WindsorCastle;
using Box.Data.Core;
using Box.Data.NHibernate.Repository;
using BoxExample.NameModule.Domain.Estados;
using BoxApp.Data.NHibernate;
using BoxApp.NameModule.Data.Repositories;
using BoxExample.NameModule.Domain.Municipios;
using BoxExample.NameModule.Aplication.EstadoManagement;

namespace BoxExample.IoC.WindsorCastle
{
    public class IoCWebContainer : AWindsorCastleContainer
    {
        public IoCWebContainer() : base()
        {

        }

        protected override void ConfigureContainer()
        {
            container.Register(Component.For<IQueryableUnitOfWork>().ImplementedBy<BoxExampleUnitOfWork>().LifeStyle.Singleton);
            container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)).LifeStyle.Transient);
            container.Register(Component.For(typeof(IFetchRepository<>)).ImplementedBy(typeof(HibernateRepository<>)).LifeStyle.Transient);

            #region Domains

            #endregion

            #region Repositories
            // Base
            container.Register(Component.For<IEstadoRepository>().ImplementedBy<EstadoRepository>().LifeStyle.Transient);
            container.Register(Component.For<IMunicipioRepository>().ImplementedBy<MunicipioRepository>().LifeStyle.Transient);

            #endregion

            #region Applications
            // Base
            container.Register(Component.For<IEstadoManagementServices>().ImplementedBy<EstadoManagementServices>().LifeStyle.Transient);

            #endregion
        }

        public static void BuildMapping()
        {
            BoxExampleSessionFactory.BuildMapping();
        }
    }
}
